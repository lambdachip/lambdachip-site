const path = require('path');
const webpack = require('webpack');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

const IS_DEV = (process.env.NODE_ENV === 'development');

module.exports = {
  watch: false,
  entry:
    {
      index: './src/index.jsx',
      editor: './src/editor.jsx',
      dashboard: './src/dashboard.jsx',
      upload: './src/upload.jsx',
      docs: './src/docs.jsx',
      toc: './src/toc.jsx',
      news: './src/news.jsx',
    },
  output: {
    // filename: '[name].bundle.js',
    // Use cache hash，and with WebpackManifestPlugin outputing the file name mappings
    filename: IS_DEV ? '[name].bundle.js' : '[name].bundle.[chunkhash].js',
    path: `${__dirname}/pub/js`,
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env.BABEL_ENV': JSON.stringify(process.env.BABEL_ENV),
    }),
    // Generate a manifest.json for filename entry mappings
    new WebpackManifestPlugin({
      fileName: `${__dirname}/pub/manifest.json`,
      publicPath: '', // webpack 5 needs to set a default publicPath
    }),
    IS_DEV ? new BundleAnalyzerPlugin() : new webpack.DefinePlugin({}),
  ],

  // Enable sourcemaps for debugging webpack's output.
  devtool: 'source-map',
  resolve: {
    alias: {
      scripts: path.resolve(__dirname, 'src'),
    },
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
  },

  module: {
    rules: [
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      { enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: IS_DEV,
          },
        },
      },
    ],
  },

  // modules: {
  //     resolve.fallback: { "path": false }
  // },

  // When importing a module whose path matches one of the following, just
  // assume a corresponding global variable exists and use that instead.
  // This is important because it allows us to avoid bundling all of our
  // dependencies, which allows browsers to cache those libraries between builds.
  externals: {
    'highlight.js': 'hljs',
    // "react": "React",
    // "react-dom": "ReactDOM"
  },
  optimization: {
    minimize: !IS_DEV, // <---- disables uglify.
    // minimizer: [new UglifyJsPlugin()] if you want to customize it.
    runtimeChunk: 'single',

    // it's not convenient to use split chunks in the current artanis model
    splitChunks: {
      // minChunks: 2,
      cacheGroups: {
        // recharts: {
        //   name: 'recharts',
        //   test: /[\\/]node_modules[\\/]recharts[\\/]/,
        //   priority: -11,
        //   minChunks: 2,
        //   reuseExistingChunk: true,
        //   chunks: 'all',
        // },
        materialui: {
          name: 'material-ui',
          test: /[\\/]node_modules[\\/]@material-ui[\\/]/,
          minChunks: 2,
          reuseExistingChunk: true,
          chunks: 'all',
        },
        react: {
          name: 'react',
          test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
          minChunks: 2,
          chunks: 'all',
        },
        editorVendor: {
          name: 'editor_vendor',
          test: /[\\/]node_modules[\\/](marked)[\\/]/,
          minChunks: 2,
          chunks: 'all',
        },
        vendors: {
          name: 'vendors',
          test: /[\\/]node_modules[\\/]/,
          minChunks: 5, // 设得较大，等于多个entry共同拥有的
          priority: -20,
          chunks: 'all',
        },
      //   vendors: {
      //     test: /[\\/]node_modules[\\/]/,
      //     name: 'vendors',
      //     minChunks: 2,
      //     reuseExistingChunk: true,
      //     chunks: 'initial',
      //   },
      },
    },

  },
};

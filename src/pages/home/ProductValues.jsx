import React from 'react';
import PropTypes from 'prop-types';
// import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import {
  Link,
  Grid,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  CardActionArea,
  IconButton,
  Avatar,
  Collapse,
} from '@material-ui/core';
// import { red } from '@material-ui/core/colors';
// import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
// import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// import MoreVertIcon from '@material-ui/icons/MoreVert';
import Container from '@material-ui/core/Container';

import { toDateTime } from 'scripts/utils/datetime';
import Typography from 'scripts/modules/components/mui/Typography';

const styles = (theme) => ({
  media: {
    height: 65,
    width: 80,
    // paddingTop: '56.25%', // 16:9
  },
  root: {
    // maxWidth: 345,
    display: 'flex',
    overflow: 'hidden',
  },
  container: {
    // marginTop: theme.spacing(10),
    marginBottom: theme.spacing(15),
    // minWidth: 800,
    // maxWidth: 1600,
    display: 'flex',
    position: 'relative',
    flexDirection: 'column',
    alignItems: 'center',
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(0, 5),
  },
  title: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
  },
  curvyLines: {
    pointerEvents: 'none',
    position: 'absolute',
    top: -180,
  },
});

const ValueCard = ({ card }) => (
  <Grid item xs={12} md={4}>
    <CardActionArea href={card.url}>
      <Card className={styles.root}>
        <CardMedia
          className={styles.media}
          component="img"
          image={card.pic}
        />
        <CardHeader
          title={(
            <Typography variant="newstitle" component="h5">
              {card.title}
            </Typography>
                      )}
          subheader={(
            <Typography variant="newsdate" component="h6">
              {toDateTime(card.date)}
            </Typography>
                              )}
        />
        <CardContent>
          <Typography variant="newscontent" component="p">
            {card.abstract}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton
            aria-label="share"
            className={styles.button}
            component="a"
            href={`https://twitter.com/share?url=${card.url}`}
          >
            <ShareIcon />
          </IconButton>
        </CardActions>
      </Card>
    </CardActionArea>
  </Grid>
);

class News extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: 'nothing',
    };
  }

  componentDidMount() {
    fetch('/news/latest/3')
      .then((res) => res.json())
      .then(
        (result) => {
          const { news } = result;
          const v = news.map((e) => (
            <ValueCard card={e} />
          ));
          if (news.length > 0) {
            this.setState({
              isLoaded: true,
              data: v,
            });
          } else {
            return [];
          }
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
  }

  render() {
    const { error, isLoaded, data } = this.state;

    if (error) {
      return (
        <div>
          Error:
          {error.message}
        </div>
      );
    } if (!isLoaded) {
      return <div>Loading...</div>;
    }
    return data;
  }
}

const ProductValues = (props) => {
  const { classes } = props;

  return (
    <section className={classes.root}>
      <Container className={classes.container}>
        <Typography variant="h4" marked="center" className={classes.title} component="h2">
          News
        </Typography>
        <div>
          <Grid container spacing={4}>
            <News />
          </Grid>
        </div>
      </Container>
    </section>
  );
};

ProductValues.propTypes = {
  classes: PropTypes.shape({}).isRequired,
};

export default withStyles(styles)(ProductValues);

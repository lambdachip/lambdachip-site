import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Button from 'scripts/modules/components/mui/Button';
import Typography from 'scripts/modules/components/mui/Typography';
import ProductHeroLayout from './ProductHeroLayout';

const backgroundImage = '/img/bg.png';

const styles = (theme) => ({
    background: {
        backgroundImage: `url(${backgroundImage})`,
        backgroundColor: '#7fc7d9', // Average color of the background image.
        backgroundPosition: 'center',
    },
    button: {
        color: theme.palette.common.white,
        minWidth: 200,
    },
    h5: {
        marginBottom: theme.spacing(4),
        marginTop: theme.spacing(4),
        [theme.breakpoints.up('sm')]: {
            marginTop: theme.spacing(10),
        },
        fontSize: 30,
    },
    more: {
        marginTop: theme.spacing(2),
    },
});

function ProductHero(props) {
    const { classes } = props;

    return (
        <ProductHeroLayout backgroundClassName={classes.background}>
            {/* Increase the network loading priority of the background image. */}
            <img style={{ display: 'none' }} src={backgroundImage} alt="increase priority" />
            <br />
            <Typography color="inherit" align="center" variant="h2" marked="center">
                Functional programming
                <br />
                on
                <br />
                embedded system
            </Typography>
            <Typography color="inherit" align="center" variant="h6" className={classes.h6} />
            <Typography color="inherit" align="center" variant="h5" className={classes.h5}>
                100% free software + free hardware design
            </Typography>
            <Button
                color="secondary"
                variant="outlined"
                size="large"
                component="a"
                href="https://lambdachip.com/articles/misc/buy"
            >
                Get DevKit
            </Button>
            <Typography variant="body2" align="center" color="inherit" className={classes.more}>
                Rapid, Robustness, Roll-out
            </Typography>
        </ProductHeroLayout>
    );
}

ProductHero.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductHero);

import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

import Button from 'scripts/modules/components/mui/Button';
import Typography from 'scripts/modules/components/mui/Typography';

const styles = (theme) => ({
  root: {
    display: 'flex',
    overflow: 'hidden',
  },
  container: {
    marginTop: theme.spacing(10),
    marginBottom: theme.spacing(15),
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(0, 2),
  },
  title: {
    marginBottom: theme.spacing(14),
  },
  number: {
    fontSize: 24,
    fontFamily: theme.typography.fontFamily,
    color: theme.palette.secondary.main,
    fontWeight: theme.typography.fontWeightMedium,
  },
  imageContainer: {
    minHeight: 225,
  },
  image: {
    maxWidth: '100%',
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  curvyLines: {
    pointerEvents: 'none',
    position: 'absolute',
    top: -180,
    opacity: 0.7,
  },
  button: {
    color: theme.palette.common.white,
    marginTop: theme.spacing(8),
  },
});

const HowItWorksCard = (props) => {
  const {
    classes,
    stepNumber,
    stepImgSrc,
    stepImgAlt,
    stepDesc,
  } = props;
  return (
    <Grid item xs={12} md={4}>
      <div className={classes.item}>
        <div className={classes.number}>
          {stepNumber}
          .
        </div>
        <div className={classes.imageContainer}>
          <img
            src={stepImgSrc}
            alt={stepImgAlt}
            className={classes.image}
          />
        </div>
        <Typography variant="h5" align="left">
          {stepDesc}
        </Typography>
      </div>
    </Grid>
  );
};

function ProductHowItWorks(props) {
  const { classes } = props;

  const stepDataList = [
    {
      stepNumber: 1,
      stepImgSrc: '/img/step1.png',
      stepImgAlt: 'Compile source code',
      stepDesc: 'Laco compile is designed for functional programming featured languages. Laco compiler generates LEF bytecode.',
    },
    {
      stepNumber: 2,
      stepImgSrc: '/img/step2.png',
      stepImgAlt: 'Copy bytecode file to TF card',
      stepDesc: 'Copy bytecode file to TF card.',
    },
    {
      stepNumber: 3,
      stepImgSrc: '/img/step3.png',
      stepImgAlt: 'Insert TF card to PCB board and power up',
      stepDesc: 'Insert TF card to PCB board and power up. The program will run automatically.',
    },
  ];

  return (
    <section className={classes.root}>
      <Container className={classes.container}>
        <Typography variant="h4" marked="center" className={classes.title} component="h2">
          How it works
        </Typography>
        <div>
          <Grid container spacing={5}>
            {
                  stepDataList.map((cardData) => (
                    <HowItWorksCard
                      key={cardData.stepNumber}
                      classes={classes}
                      {...cardData}
                    />
                  ))
              }
          </Grid>
        </div>
        <Button
          color="secondary"
          size="large"
          variant="contained"
          className={classes.button}
          component="a"
          href="/articles/docs/0"
        >
          Get started
        </Button>
      </Container>
    </section>
  );
}

ProductHowItWorks.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductHowItWorks);

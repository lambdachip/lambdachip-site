import React from 'react';

import AppFooter from 'scripts/modules/views/AppFooter';
import { HomeTopbar } from 'scripts/modules/layout/topbar/Topbar';

import ProductHero from './ProductHero';
import ProductValues from './ProductValues';
import ProductHowItWorks from './ProductHowItWorks';
import ProductSmokingHero from './ProductSmokingHero';

export const Home = () => (
  <>
    <HomeTopbar />
    <ProductHero />
    <ProductHowItWorks />
    <ProductValues />
    <ProductSmokingHero />
    <AppFooter />
  </>
);

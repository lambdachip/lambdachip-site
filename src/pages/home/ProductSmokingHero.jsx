import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/core/styles';

import Typography from 'scripts/modules/components/mui/Typography';

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: theme.spacing(9),
    marginBottom: theme.spacing(9),
  },
  button: {
    border: '4px solid currentColor',
    borderRadius: 0,
    height: 'auto',
    padding: theme.spacing(2, 5),
  },
  link: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  buoy: {
    width: 60,
  },
});

function ProductSmokingHero(props) {
  const { classes } = props;

  return (
    <Container className={classes.root} component="section" data-name="smoking-hero">
      <Button className={classes.button}>
        <Typography variant="h4" component="span">
          Need help?
        </Typography>
      </Button>
      <Typography variant="h5" className={classes.link}>
        For the pontential partners, please send emails to us.
        We rely on email, and we check email everyday.
        We may reply the meaningful emails.
      </Typography>
      <Typography variant="h5" className={classes.link}>
        We are looking for market channels, please contact us:
      </Typography>
      <Typography variant="body1" color="secondary" className={classes.link}>
        business@lambdachip.com
      </Typography>
      <Typography variant="h5" className={classes.link}>
        For other questions:
      </Typography>
      <Typography variant="body1" color="secondary" className={classes.link}>
        help@lambdachip.com
      </Typography>
    </Container>
  );
}

ProductSmokingHero.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductSmokingHero);

import React from 'react';
import ReactDOM from 'react-dom';
import { withRoot } from './modules/withRoot';
import { Home } from './pages/home';

const App = withRoot(Home);
ReactDOM.render(<App />, document.getElementById('root'));

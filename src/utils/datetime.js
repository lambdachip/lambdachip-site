/**
 * Formatting datetime output from seconds
 */
export const toDateTime = (secs) => {
  const date = new Date(0);
  date.setSeconds(secs);
  return date.toDateString();
};

import React from 'react';
import ReactDOM from 'react-dom';
import dayjs from 'dayjs';
import { flow } from 'lodash-es';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {
  withTheme, // withStyles,
} from '@material-ui/core/styles';

import { ArticleDao } from './dao/logic/article';
import { MarkdownViewer } from './modules/components/MarkdownViewer';

import { View404 } from './modules/views/HTMLErrorViews';
import { HomeTopbar } from './modules/layout/topbar/Topbar';
import AppFooter from './modules/views/AppFooter';
import { withRoot } from './modules/withRoot';
import theme from './theme';

class DocumentView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDataLoaded: false,
      isMetaLoaded: false,
      data: null,
      meta: null,
      error: null,
    };
  }

  componentDidMount() {
    const { category, oid } = this.props;
    this.loadPageData(category, oid);
  }

  loadPageData(category, oid) {
    this.loadPageMeta(category, oid);
  }

  loadPageMeta(category, oid) {
    return ArticleDao.loadPageMeta(category, oid)
      .then((meta) => {
        this.setState({
          isMetaLoaded: true,
          meta,
        });

        return this.loadPageContent(meta);
      })
      .catch((error) => {
        this.setState({
          isMetaLoaded: true,
          error,
        });
      });
    // return fetch(`/${category}/${oid}/meta.json`)
    //   .then((res) => {
    //     if (res.status === 404) {
    //       this.setState({
    //         isMetaLoaded: true,
    //         error: 404,
    //       });
    //       return null;
    //     }
    //     // set meta and load content
    //     return res.json();
    //   })
    //   .then((meta) => {
    //     this.setState({
    //       isMetaLoaded: true,
    //       meta,
    //     });
    //     return this.loadPageContent(category, oid, (meta && meta.mtime));
    //   })
    //   .catch((error) => {
    //     this.setState({
    //       isMetaLoaded: true,
    //       error,
    //     });
    //   });
  }

  loadPageContent(meta) {
    return ArticleDao.loadPageContent(
      meta.getCategory(),
      meta.getId(),
      meta.getUpdatedTime(),
    )
      .then((result) => {
        this.setState({
          isDataLoaded: true,
          data: result,
        });
      })
      .catch((error) => {
        this.setState({
          isDataLoaded: true,
          error,
        });
      });
  }

  renderTitleMessage(title) {
    // const { theme } = this.props;
    return (
      <Paper
        elevation={6}
      >
        <Typography
          align="center"
          variant="body2"
          component="p"
          classes={theme.typography.title}
        >
          { title }
        </Typography>
      </Paper>
    );
  }

  renderError() {
    const { error } = this.state;
    let errorContent;
    if (error === 404) {
      errorContent = (<View404 />);
    } else {
      errorContent = (
        <div>
          Error:
          {error.message}
        </div>
      );
    }

    return this.renderTitleMessage(errorContent);
  }

  renderLoading() {
    return this.renderTitleMessage(
      <div>Loading...</div>,
    );
  }

  renderMarkdown(beforeArticle, afterArticle) {
    // const { theme } = this.props;
    const { meta, data } = this.state;
    return (
      <Paper
        elevation={6}
      >
        <Container style={{
          paddingTop: 20,
          paddingBottom: 20,
        }}
        >
          <Typography
            align="center"
            variant="h4"
            component="h4"
            classes={theme.typography.title}
            paragraph
          >
            {meta.getTitle()}
          </Typography>

          {beforeArticle}

          <MarkdownViewer
            content={data}
          />

          {afterArticle}
        </Container>
      </Paper>
    );
  }

  renderNews() {
    const { meta } = this.state;
    let beforeArticle = null;
    const createdTime = meta.getCreatedTime();
    const author = meta.getAuthor();
    if (createdTime) {
      let updateTimeText = '';
      if (author) {
        updateTimeText += `written by ${author}, `;
      }
      if (createdTime) {
        const dateText = dayjs.unix(meta.getCreatedTime()).format('MMM D, YYYY h:mm A');
        updateTimeText += `${dateText}`;
      }

      beforeArticle = (
        <p style={{ textAlign: 'right' }}>
          <i>
            <sub>
              {updateTimeText}
            </sub>
          </i>
        </p>
      );
    }

    return this.renderMarkdown(beforeArticle, null);
  }

  renderDocs() {
    const { meta } = this.state;
    // 在docs最后显示更新时间和更新者
    let afterArticle = null;
    if (meta.getUpdatedTime()) {
      const dateText = dayjs.unix(meta.getUpdatedTime()).format('MMM D, YYYY h:mm A');
      let updateTimeText = `(Updated at ${dateText}.)`;
      if (meta.getAuthor()) {
        updateTimeText = `(Updated by ${meta.getAuthor()} at ${dateText}.)`;
      }
      afterArticle = (
        <p>
          <i><sub>{updateTimeText}</sub></i>
        </p>
      );
    }

    return this.renderMarkdown(null, afterArticle);
  }

  renderArticle() {
    const { meta } = this.state;
    if (!meta) {
      return null;
    } if (meta.getCategory() === 'news') {
      return this.renderNews();
    }

    // category === 'docs' or else
    return this.renderDocs();
  }

  render() {
    const {
      isDataLoaded, isMetaLoaded, error,
    } = this.state;

    let content = null;
    if (error) {
      content = this.renderError();
    } else if (!isDataLoaded || !isMetaLoaded) {
      content = this.renderLoading();
    } else {
      content = this.renderArticle();
    }

    return (
      <>
        <HomeTopbar />
        <Container style={{
          marginTop: 10,
          marginBottom: 10,
        }}
        >
          { content }
        </Container>
        <AppFooter />
      </>
    );
  }
}

const article = document.getElementById('article-content');

const withArticle = (Component) => (props) => {
  const oid = article.getAttribute('oid');
  const category = article.getAttribute('category');

  return (
    <Component
      {...props}
      oid={oid}
      category={category}
    />
  );
};

const Doc = flow(
  withTheme,
  withArticle,
  withRoot,
)(DocumentView);

ReactDOM.render(<Doc />, article);

/*
  主页布局的顶栏
 */

import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid, Link, Hidden, AppBar, Toolbar,
} from '@material-ui/core';

// import AppBar from '../components/AppBar';
// import Toolbar, { styles as toolbarStyles } from '../components/Toolbar';

import { TopbarNavItems } from './data/TopbarNavItems';

const getTopbarHeight = (theme) => ({
  minHeight: 64,
  [theme.breakpoints.up('sm')]: {
    minHeight: 70,
  },
  [theme.breakpoints.down('sm')]: {
    minHeight: 128,
  },
});

const styles = (theme) => ({
  title: {
    fontSize: 24,
  },
  placeholder: {
    ...getTopbarHeight(theme),
  },
  toolbar: {
    ...getTopbarHeight(theme),
    color: theme.palette.common.white,
    justifyContent: 'space-between',
  },
  left: {
    flex: 1,
    display: 'flex',
    minHeight: theme.topbar.minHeight,
    // marginBottom: 0,
  },
  leftLink: {
    display: 'flex',
    alignItems: 'center',
  },
  leftLinkActive: {
    color: theme.palette.common.white,
  },
  right: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
    minHeight: 64,
    marginBottom: 0,
    [theme.breakpoints.down('sm')]: {
      minHeight: 64,
    },
  },
  rightExpand: {
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    [theme.breakpoints.down('xs')]: {
      flexWrap: 'wrap',
    },
  },
  rightLink: {
    fontSize: 16,
    color: theme.palette.common.white,
    '&:not(:first-child)': {
      marginLeft: theme.spacing(3),
    },
  },
  lambdachip: {
    fontSize: 25,
    color: theme.palette.common.white,
    marginLeft: theme.spacing(3),
  },
  linkSecondary: {
    color: theme.palette.secondary.main,
  },
});

const LargeScreenMenu = ({ classes, navItems }) => (
  <Grid container className={clsx(classes.right)} alignItems="center">
    {
      navItems.map((item) => (
        <Link
          variant="h6"
          underline="none"
          color="white"
          className={item.getClassName(classes)}
          href={item.link}
        >
          <img src={item.img} alt={item.label} />
          {item.label}
        </Link>
      ))
    }
  </Grid>
);

const SmallScreenMenu = ({ classes, navItems }) => (
  <Grid container className={clsx(classes.right, classes.rightExpand)} alignItems="center">
    {
      navItems.map((item) => (
        <Link
          variant="h6"
          underline="none"
          color="white"
          className={item.getClassName(classes)}
          href={item.link}
        >
          <img src={item.img} alt={item.label} />
          {item.label}
        </Link>
      ))
    }
  </Grid>
);

const AppTopBar = ({ classes }) => (
  <div>
    <AppBar elevation={0} position="fixed">
      <Toolbar className={classes.toolbar}>
        <Grid container>
          {/* Left Logo */}
          <Grid
            item
            xs={12}
            md={4}
            className={classes.left}
          >
            <Link
              variant="h6"
              underline="none"
              className={classes.leftLink}
              href="/"
            >
              <img src="/img/logo.png" alt="logo" width="50" height="50" />
              <span className={classes.lambdachip}>LambdaChip</span>
            </Link>
          </Grid>
          {/* large screens */}
          <Grid
            item
            xs={12}
            md={8}
          >
            <Hidden smDown>
              <LargeScreenMenu classes={classes} />
            </Hidden>
            {/* small screens */}
            <Hidden mdUp>
              <SmallScreenMenu
                classes={classes}
                navItems={TopbarNavItems}
              />
            </Hidden>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
    <div className={classes.placeholder} />
  </div>
);

AppTopBar.propTypes = {
  classes: PropTypes.shape({}).isRequired,
};

export default withStyles(styles)(AppTopBar);

import clsx from 'clsx';

/**
 * 网站顶栏的菜单配置（数据和视图分离）
 *
 * @type {Array}
 */
export const TopbarNavItems = [
  {
    label: 'DevKit',
    itemStyle: 'primary',
    subItems: [
      {
    label: 'Shop',
    link: 'https://lambdachip.com/articles/misc/buy',
      },
      {
        label: 'Get Started',
        link: '/articles/docs/0',
        itemStyle: 'primary',
      },
      {
        label: 'Documents',
        link: '/articles/docs',
        itemStyle: 'primary',
      },
    ],
  },
  {
    label: 'News',
    link: '/articles/news',
    itemStyle: 'primary',
  },
  {
    label: 'Developers',
    subItems: [
      {
        label: 'Source Code',
        link: '/articles/misc/repo',
        itemStyle: 'primary',
      },
      {
        label: 'Download',
        link: '/articles/misc/download',
        itemStyle: 'primary',
      },
    ],
  },
  {
    label: 'Business',
    subItems: [
      {
        label: 'Solution',
        link: '/articles/misc/solution',
        itemStyle: 'primary',
      },
      {
        label: 'Whitepaper',
        link: '/articles/misc/whitepaper',
        itemStyle: 'primary',
      },
    ],
  },
  {
    label: 'Forum',
    link: 'https://reddit.com/r/lambdachip',
    itemStyle: 'primary',
  },
  {
    label: 'RSS',
    img: '/img/feed-icon-14x14.png',
    link: '/feed/atom',
    itemStyle: 'imagelink',
  },
];

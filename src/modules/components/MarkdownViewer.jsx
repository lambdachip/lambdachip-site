import React from 'react';
import PropTypes from 'prop-types';
import { createMarkdown } from 'safe-marked';
import hljs from 'highlight.js';

const markdown = createMarkdown();

// initialize highlight.js
hljs.configure({
  tabReplace: '  ', // 2 spaces
});

/**
 * MarkdownViewer for Web Site's Documents
 */
export class MarkdownViewer extends React.PureComponent {
  static propTypes = {
    content: PropTypes.string.isRequired,
  }

  componentDidMount() {
    this.replaceStyles();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.content !== this.props.content) {
      this.replaceStyles();
    }
  }

  onSetRefContent = (node) => {
    this.refContent = node;
  };

  replaceStyles() {
    if (this.refContent) {
      // replace link styles:
      // this.refContent
      //   .querySelectorAll('a')
      //   .forEach((e) => { e.style.color = '#ff6f61'; });

      // don't query something is not pre-rendered
      this.refContent.querySelectorAll('code').forEach((block) => {
        hljs.highlightBlock(block);
      });
    }
  }

  render() {
    const { content } = this.props;
    const md = markdown(content);
    return (
      <div
        className="doc-content"
        ref={this.onSetRefContent}
        dangerouslySetInnerHTML={{ __html: md }}
      />
    );
  }
}

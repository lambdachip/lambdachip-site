/**
 * A list of error views
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
  cardTitle: {
    color: '#ff6f61',
    fontSize: 30,
    fontFamily: "'Open Sans', sans-serif",
  },
});

// HTTP 404
export const View404 = withStyles(styles)(
  ({ classes }) => (
    <div id="main" className="row" style={{ textAlign: 'left', padding: 20 }}>
      <div className="col s12">
        <div className="card">
          <div className="card-content">
            <span className={classes.cardTitle}>
              <font color="#ff6f61" size="30px">Page not found :(</font>
            </span>
            <p><font color="#263056">Here are possible reasons:</font></p>
            <p><font color="#263056">1. Maybe the page you are looking for has been removed.</font></p>
            <p><font color="#263056">2. It's still under construction.</font></p>
            <p><font color="#263056">3. You typed wrong URL.</font></p>
            <p><font color="#263056">Anyway, we encourage you check your input or take a look at other pages.</font></p>
          </div>
        </div>
      </div>
      <a href="/"><font color="#ff6f61">Back to lambdachip.com</font></a>
    </div>
  ),
);

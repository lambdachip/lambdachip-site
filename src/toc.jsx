import React from 'react';
import ReactDOM from 'react-dom';
// import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
// import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

import { MarkdownViewer } from './modules/components/MarkdownViewer';
import { HomeTopbar } from './modules/layout/topbar/Topbar';
import AppFooter from './modules/views/AppFooter';
import { withRoot } from './modules/withRoot';
import theme from './theme';

class MarkdownToc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMetaLoaded: false,
      meta: 'no meta',
      error: null,
    };
  }

  componentDidMount() {
    fetch(`/articles/${this.props.category}/all`)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isMetaLoaded: true,
            meta: result,
          });
        },
        (error) => {
          this.setState({
            isMetaLoaded: true,
            error,
          });
        },
      );
  }

  renderTOC() {
    const { meta } = this.state;
    let toc = '';
    meta.docs.forEach((d) => {
      const brains = '&#x1F9E0;'.repeat(d.difficulty);
      toc += `[${d.oid}.&nbsp;&nbsp;&nbsp;${d.title}](/articles/${d.category}/${d.oid})&nbsp;${brains}\n\n`;
    });

    return (
      <Container
        data-section="toc"
        style={{
          paddingTop: 20,
          paddingBottom: 20,
        }}
      >
        <MarkdownViewer content={toc} />
      </Container>
    );
  }

  render() {
    const {
      isMetaLoaded, error,
    } = this.state;

    if (error) {
      return (
        <div>
          Error:
          {error.message}
        </div>
      );
    } if (!isMetaLoaded) {
      return <div>Loading...</div>;
    }

    return (
      <>
        <HomeTopbar />
        <Typography align="center" variant="h4" component="h6" className={theme.typography.title} gutterBottom>
          Table of content
        </Typography>
        <Container style={{
          marginTop: 10,
          marginBottom: 10,
        }}
        >
          <Paper
            elevation={6}
          >
            {this.renderTOC()}
          </Paper>
        </Container>
        <AppFooter />
      </>
    );
  }
}

const article = document.getElementById('article-content');
const Toc = withRoot(MarkdownToc);

ReactDOM.render(<Toc category={article.getAttribute('category')} />,
  article);

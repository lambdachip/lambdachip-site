import { get } from 'lodash-es';

class ArticleMeta {
  constructor(meta = {}) {
    this.data = {
      oid: '',
      ...meta,
    };
  }

  getId() {
    return get(this.data, 'oid');
  }

  getTitle() {
    return get(this.data, 'title');
  }

  getCreatedTime() {
    return get(this.data, 'ctime');
  }

  getUpdatedTime() {
    return get(this.data, 'mtime');
  }

  getCategory() {
    return get(this.data, 'category');
  }

  getAuthor() {
    return get(this.data, 'author');
  }
}

// Data parser for "docs"
class DocMeta extends ArticleMeta {

}

// Data parser for "news"
class NewsMeta extends ArticleMeta {
  constructor(meta = {}) {
    super(meta);

    // news default author
    if (!this.data.author) {
      this.data.author = 'Terry';
    }
  }

  getCreatedTime() {
    return get(this.data, 'date');
  }

  getUpdatedTime() {
    return get(this.data, 'date');
  }
}

const ArticleMetaFactory = new class {
  // eslint-disable-next-line class-methods-use-this
  create(category, meta) {
    if (category === 'docs') {
      return new DocMeta(meta);
    } if (category === 'news') {
      return new NewsMeta(meta);
    }
    return new ArticleMeta(meta);
  }
}();

export {
  ArticleMeta,
  DocMeta,
  NewsMeta,
  ArticleMetaFactory,
};

import * as ArticleDao from './dao';
import * as ArticleModel from './model';

export { ArticleDao, ArticleModel };

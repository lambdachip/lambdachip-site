import { ArticleMetaFactory } from './model';

export const loadPageMeta = (category, oid) => {
  const entry = `/${category}/${oid}/meta.json`;
  return fetch(entry)
    .then((res) => {
      if (res.status === 404) {
        throw new Error(res);
      }
      return res.json();
    })
    .then((data) => ArticleMetaFactory.create(category, data));
};

export const loadPageContent = (category, oid, stamp) => {
  const entry = `/${category}/${oid}/content.md?t=${stamp}`;
  return fetch(entry)
    .then((res) => {
      if (res.status === 404) {
        throw new Error(res);
      }
      return res.text();
    });
};

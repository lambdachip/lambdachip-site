import React from 'react';
import { render } from 'react-dom';
import { makeStyles } from '@material-ui/styles';
import Button from '@material-ui/core/Button';

// 在哪里用的？？
function handleImageUpload(event, theFile) {
  event.stopPropagation();
  event.preventDefault();

  const formData = new FormData();
  formData.append('file', theFile);
  fetch('/dashboard/upload', {
    method: 'POST',
    body: formData,
    headers: {
      credentials: 'include',
    },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
    })
    .catch((error) => {
      console.error(error);
    });
}

/**
 * @deprecated this file is not used anymore. use this instead: /src/theme/index.js
 */

import { createMuiTheme } from '@material-ui/core/styles';
import { green, grey, red } from '@material-ui/core/colors';

/**
 * Theme主题的作用是定义通用的全局样式，尤其是常量和通用类。
 *
 * 常量包括大小、颜色、字体等。
 * 通用类包括标题、正文、特殊用途文字等。
 *
 * 组件的样式应该在各组件内定义，可以通过引用主题的常量来定义组件里的块样式。
 */
const rawTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#263056',
    },
    secondary: {
      main: '#ff6f61',
    },
    warning: {
      main: '#ffc071',
      dark: '#ffb25e',
    },
    error: {
      xLight: red[50],
      main: red[500],
      dark: red[700],
    },
    success: {
      xLight: green[50],
      main: green[500],
      dark: green[700],
    },
  },
  typography: {
    fontFamily: "'Work Sans', sans-serif",
    fontSize: 18,
    fontWeightLight: 399, // Work Sans
    fontWeightRegular: 500, // Work Sans
    fontWeightMedium: 700, // Roboto Condensed
    fontFamilySecondary: "'Roboto Condensed', sans-serif",
  },
});

const fontHeader = {
  color: rawTheme.palette.secondary.main,
  fontWeight: rawTheme.typography.fontWeightMedium,
  fontFamily: rawTheme.typography.fontFamilySecondary,
  textTransform: 'uppercase',
};

const theme = {
  ...rawTheme,
  palette: {
    ...rawTheme.palette,
    background: {
      ...rawTheme.palette.background,
      default: rawTheme.palette.common.white,
      placeholder: grey[200],
    },
  },
  title: {
    color: rawTheme.palette.secondary.main,
    fontWeight: rawTheme.typography.fontWeightMedium,
    fontFamily: rawTheme.typography.fontFamilySecondary,
    letterSpacing: 0,
    fontSize: 60,
  },
  typography: {
    ...rawTheme.typography,
    fontHeader,
    title: {
      color: rawTheme.palette.secondary.main,
      fontWeight: rawTheme.typography.fontWeightMedium,
      fontFamily: rawTheme.typography.fontFamilySecondary,
      letterSpacing: 0,
      fontSize: 60,
    },
    h1: {
      ...rawTheme.typography.h1,
      ...fontHeader,
      letterSpacing: 0,
      fontSize: 60,
    },
    h2: {
      ...rawTheme.typography.h2,
      ...fontHeader,
      fontSize: 48,
    },
    h3: {
      ...rawTheme.typography.h3,
      ...fontHeader,
      fontSize: 42,
    },
    h4: {
      ...rawTheme.typography.h4,
      ...fontHeader,
      fontSize: 30,
    },
    h5: {
      ...rawTheme.typography.h5,
      fontSize: 20,
      fontWeight: rawTheme.typography.fontWeightLight,
    },
    h6: {
      ...rawTheme.typography.h6,
      ...fontHeader,
      fontSize: 18,
      textTransform: 'none',
      color: rawTheme.palette.common.white,
    },
    subtitle1: {
      ...rawTheme.typography.subtitle1,
      color: rawTheme.palette.primary.main,
      fontSize: 22,
    },
    body1: {
      ...rawTheme.typography.body2,
      fontWeight: rawTheme.typography.fontWeightRegular,
      fontSize: 20,
      color: rawTheme.palette.primary.main,
    },
    body2: {
      ...rawTheme.typography.body1,
      color: rawTheme.palette.primary.main,
      fontSize: 18,
    },
    newstitle: {
      ...rawTheme.typography.h3,
      ...fontHeader,
      color: rawTheme.palette.primary.main,
      fontSize: 13,
    },
    newsdate: {
      ...rawTheme.typography.h5,
      color: rawTheme.palette.common.primary,
      fontSize: 10,
    },
    newscontent: {
      color: rawTheme.palette.common.primary,
      fontSize: 13,
    },
    doctitle: {
      ...rawTheme.typography.h4,
      ...fontHeader,
      color: rawTheme.palette.common.secondary,
      fontSize: 20,
    },
  },
  topbar: {
    minHeight: 64,
  },
};

export default theme;

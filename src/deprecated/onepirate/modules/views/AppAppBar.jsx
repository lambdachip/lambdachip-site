import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Hidden from '@material-ui/core/Hidden';
import AppBar from '../components/AppBar';
import Toolbar, { styles as toolbarStyles } from '../components/Toolbar';

const styles = (theme) => ({
  title: {
    fontSize: 24,
  },
  placeholder: toolbarStyles(theme).root,
  toolbar: {
    justifyContent: 'space-between',
  },
  left: {
    flex: 1,
    display: 'flex',
    minHeight: theme.topbar.minHeight,
    // marginBottom: 0,
  },
  leftLink: {
    display: 'flex',
    alignItems: 'center',
  },
  leftLinkActive: {
    color: theme.palette.common.white,
  },
  right: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
    minHeight: 64,
    marginBottom: 0,
    [theme.breakpoints.down('sm')]: {
      minHeight: 64,
    },
  },
  rightExpand: {
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    [theme.breakpoints.down('xs')]: {
      flexWrap: 'wrap',
    },
  },
  rightLink: {
    fontSize: 16,
    color: theme.palette.common.white,
    '&:not(:first-child)': {
      marginLeft: theme.spacing(3),
    },
  },
  lambdachip: {
    fontSize: 25,
    color: theme.palette.common.white,
    marginLeft: theme.spacing(3),
  },
  linkSecondary: {
    color: theme.palette.secondary.main,
  },
});

const getRightLinkClass = (classes) => classes.rightLink;
const getSecondaryRightLinkClass = (classes) => clsx(classes.rightLink, classes.linkSecondary);

const TopbarNavItems = [
  {
    label: 'Shop',
    link: 'https://shop.lambdachip.com',
    getClassName: getSecondaryRightLinkClass,
  },
  {
    label: 'News',
    link: '/articles/news',
    getClassName: getRightLinkClass,
  },
  {
    label: 'Documents',
    link: '/articles/docs',
    getClassName: getRightLinkClass,
  },
  {
    label: 'Download',
    link: '/articles/misc/download',
    getClassName: getRightLinkClass,
  },
  {
    label: 'Development',
    link: '/articles/misc/repo',
    getClassName: getSecondaryRightLinkClass,
  },
  {
    label: 'Forum',
    link: 'https://reddit.com/r/lambdachip',
    getClassName: getRightLinkClass,
  },
  {
    label: '',
    img: '/img/feed-icon-14x14.png',
    link: '/feed/atom',
    getClassName: getRightLinkClass,
  },
];

const LargeScreenMenu = ({ classes }) => (
  <Grid container className={clsx(classes.right)} alignItems="center">
    {
          TopbarNavItems.map((item) => (
            <Link
              variant="h6"
              underline="none"
              color="white"
              className={item.getClassName(classes)}
              href={item.link}
            >
              <img src={item.img} />
              {item.label}
            </Link>
          ))
      }
  </Grid>
);

const SmallScreenMenu = ({ classes }) => (
  <Grid container className={clsx(classes.right, classes.rightExpand)} alignItems="center">
    {
          TopbarNavItems.map((item) => (
            <Link
              variant="h6"
              underline="none"
              color="white"
              className={item.getClassName(classes)}
              href={item.link}
            >
              <img src={item.img} />
              {item.label}
            </Link>
          ))
      }
  </Grid>
);

function AppAppBar(props) {
  const { classes } = props;

  return (
    <div>
      <AppBar position="fixed">
        <Toolbar className={classes.toolbar}>
          <Grid container>
            <Grid
              item
              xs={12}
              md={4}
              className={classes.left}
            >
              <Link
                variant="h6"
                underline="none"
                className={classes.leftLink}
                href="/"
              >
                <img src="/img/logo.png" alt="logo" width="50" height="50" />
                <span className={classes.lambdachip}>LambdaChip</span>
              </Link>
            </Grid>
            {/* large screens */}
            <Grid
              item
              xs={12}
              md={8}
            >
              <Hidden smDown>
                <LargeScreenMenu classes={classes} />
              </Hidden>
              {/* small screens */}
              <Hidden mdUp>
                <SmallScreenMenu classes={classes} />
              </Hidden>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <div className={classes.placeholder} />
    </div>
  );
}

AppAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppAppBar);

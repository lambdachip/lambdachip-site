import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import SimpleMDE from 'simplemde';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import {
  Button,
  sizing,
  TextField,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    gridGap: theme.spacing(3),
  },
  root: {
    // background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    border: 0,
    borderRadius: 3,
    //        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    height: 48,
    padding: '70px 30px',
  },
}));

ReactDOM.render(<MarkdownEditor />, document.querySelector('#editor_root'));

const mde = new SimpleMDE({
  element: document.getElementById('editor'),
  // autofocus: true,
  // blockStyles: {
  //     bold: "__",
  //     italic: "_"
  // },
  indentWithTabs: true,
  // insertTexts: {
  //     horizontalRule: ["", "\n\n-----\n\n"],
  //     image: ["![](https://", ")"],
  //     link: ["[", "](https://)"],
  //     table: ["", "\n\n| Column 1 | Column 2 | Column 3 |\n| -------- | -------- | -------- |\n| Text     | Text      | Text     |\n\n"],
  // },
  // lineWrapping: false,
  // parsingConfig: {
  //     allowAtxHeaderWithoutSpace: true,
  //     strikethrough: false,
  //     underscoresBreakWords: true,
  // },
  // placeholder: "Type here...",
  // previewRender: function(plainText, preview) { // Async method
  //     setTimeout(function(){
  //         preview.innerHTML = customMarkdownParser(plainText);
  //     }, 250);

  //     return "Loading...";
  // },
  promptURLs: true,
  renderingConfig: {
    singleLineBreaks: false,
    codeSyntaxHighlighting: true,
  },
  shortcuts: {
    drawTable: 'Cmd-Alt-T',
  },
  spellChecker: true,
  // status: ["autosave", "lines", "words", "cursor", {
  //     className: "keystrokes",
  //     defaultValue: function(el) {
  //         this.keystrokes = 0;
  //         el.innerHTML = "0 Keystrokes";
  //     },
  //     onUpdate: function(el) {
  //         el.innerHTML = ++this.keystrokes + " Keystrokes";
  //     }
  // }], // Another optional usage, with a custom status bar item that counts keystrokes
  styleSelectedText: true,
  tabSize: 2,
  // toolbar: ["bold", "italic", "heading", "|", "quote"],
  toolbarTips: true,
});

function MarkdownEditor() {
  const classes = useStyles();
  const [markdown, setMarkdown] = useState('');
  const [pub, set_pub] = useState('Publish');
  const [title, set_title] = useState('');
  const [tags, set_tags] = useState('');

  const status_change = (e) => {
    set_pub(e.target.value);
  };

  const markdown_change = (e) => {
    setMarkdown(e.target.value);
  };

  const title_change = (e) => {
    set_title(e.target.value);
  };

  const tags_change = (e) => {
    set_tags(e.target.value);
  };

  function submit_post() {
    const title = document.getElementById('article-title').value;
    const tags = document.getElementById('article-tags').value;
    const content = mde.value();
    const submit_button = document.getElementById('submit-button');
    const status = pub;

    if (title === '' || tags === '') {
      return;
    }

    submit_button.innerHTML = 'Waiting...';
    submit_button.display = true;

    fetch('/dashboard/post', {
      method: 'POST',
      body: JSON.stringify({
        title,
        tags,
      }),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
      },
    })
      .then((response) => response.text())
      .then((ret) => {
        if (ret === 'ok') {
          window.location.pathname = `/pub/docs/${oid}/content`;
        } else {
          alert('Edit failed, please check!');
          submit_button.innerText = 'Submit';
          submit_button.disabled = false;
        }
      });
  }

  function clean_post() {
    if (confirm('This will clean everything you filled, are you sure?')) {
      mde.value('');
      set_title('');
      set_tags('');
    }
  }

  return (
    <div className={classes.root}>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <TextField
            required
            error={title === ''}
            fullWidth
            id="article-title"
            placeholder="Enter your title..."
            name="title"
            onChange={title_change}
            label="Title"
          />
        </Grid>
        <Grid item width={1} xs={12}>
          <TextField
            required
            error={tags === ''}
            fullWidth
            id="article-tags"
            onChange={tags_change}
            name="tags"
            label="Tags"
          />
        </Grid>
        <Grid item width={1} xs>
          <textarea
            id="editor"
            onChange={markdown_change}
            value="start"
          />
        </Grid>
      </Grid>
      <Grid container spacing={2} xs={12}>
        <Grid item xs={2}>
          <Button
            variant="contained"
            color="primary"
            onClick={clean_post}
            id="submit-button"
          >
            Cancel
          </Button>
        </Grid>
        <Grid item xs={2}>
          <Button
            variant="contained"
            color="primary"
            onClick={submit_post}
            id="submit-button"
          >
            Submit
          </Button>
        </Grid>
        <Grid item xs={2}>
          <FormControl component="fieldset">
            <FormLabel component="legend">Publish status</FormLabel>
            <RadioGroup aria-label="Publish status" color="primary" name="status" value={pub} onChange={status_change}>
              <FormControlLabel value="Publish" control={<Radio />} label="Publish" />
              <FormControlLabel value="Private" control={<Radio />} label="Private" />
            </RadioGroup>
          </FormControl>
        </Grid>
      </Grid>
    </div>
  );
}

import React from 'react';
import { render } from 'react-dom';

import Dashboard from './pages/dashboard';

// render dashboard
render(<Dashboard />, document.querySelector('#root'));

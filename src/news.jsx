import React from 'react';
import ReactDOM from 'react-dom';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import {
  Link,
  Grid,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  CardActionArea,
  IconButton,
  Avatar,
  Collapse,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Container from '@material-ui/core/Container';
import { toDateTime } from './utils/datetime';
import Typography from './modules/components/mui/Typography';
import { HomeTopbar } from './modules/layout/topbar/Topbar';
import AppFooter from './modules/views/AppFooter';
import { withRoot } from './modules/withRoot';
import theme from './theme';

const styles = (theme) => ({
  media: {
    height: 65,
    width: 80,
    // paddingTop: '56.25%', // 16:9
  },
  root: {
    // maxWidth: 345,
    display: 'flex',
    overflow: 'hidden',
  },
  container: {
    // marginTop: theme.spacing(10),
    marginBottom: theme.spacing(15),
    // minWidth: 800,
    // maxWidth: 1600,
    display: 'flex',
    position: 'relative',
    flexDirection: 'column',
    alignItems: 'center',
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(0, 5),
    marginTop: theme.spacing(10),
  },
  title: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
  },
  curvyLines: {
    pointerEvents: 'none',
    position: 'absolute',
    top: -180,
  },
});

const ValueCard = ({ card }) => (
  <Grid item xs={12} sm={6} md={4} lg={3}>
    <CardActionArea href={card.url}>
      <Card className={styles.root}>
        <CardMedia
          className={styles.media}
          component="img"
          image={card.pic}
        />
        <CardHeader
          title={(
            <Typography variant="newstitle" component="h5">
              {card.title}
            </Typography>
              )}
          subheader={(
            <Typography variant="newsdate" component="h6">
              {toDateTime(card.date)}
            </Typography>
              )}
        />
        <CardContent>
          <Typography variant="newscontent" component="p">
            {card.abstract}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton
            aria-label="share"
            className={styles.button}
            component="a"
            href={`https://twitter.com/share?url=${card.url}`}
          >
            <ShareIcon />
          </IconButton>
        </CardActions>
      </Card>
    </CardActionArea>
  </Grid>
);

class NewsViewer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      // data: 'nothing',
      news: [],
    };
  }

  componentDidMount() {
    fetch('/news/latest/50')
      .then((res) => res.json())
      .then(
        (result) => {
          const { news } = result;
          this.setState({
            news,
            isLoaded: true,
            error: null,
          });
          // if (news.length > 0) {
          //     let v =
          //     this.setState({
          //         isLoaded: true,
          //         data: v,
          //     });
          // } else {
          //     return [];
          // }
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
  }

  renderError() {
    const { error } = this.state;
    return (
      <div>
        Error:
        {error.message}
      </div>
    );
  }

  renderLoading() {
    return (
      <div>Loading...</div>
    );
  }

  renderNews() {
    const { news } = this.state;
    const data = news.map((e) => (
      <ValueCard card={e} />
    ));

    return (
      <section className={theme.root}>
        <Container
          className={theme.container}
          minWidth="xs"
          maxWidth="lg"
          style={{
            marginTop: 10,
            marginBottom: 10,
          }}
        >
          <Paper
            elevation={6}
          >
            <Container>
              <Grid container spacing={3}>
                {data}
              </Grid>
            </Container>
          </Paper>
        </Container>
      </section>
    );
  }

  render() {
    const { error, isLoaded } = this.state;

    let content;
    if (error) {
      content = this.renderError();
    } else if (!isLoaded) {
      content = this.renderLoading();
    } else {
      content = this.renderNews();
    }

    return (
      <>
        <HomeTopbar />
        <Typography
          className={theme.typography.title}
          component="h2"
          variant="h4"
          align="center"
          gutterBottom
        >
          News
        </Typography>
        {content}
        <AppFooter />
      </>
    );
  }
}

const article = document.getElementById('article-content');
const News = withRoot(NewsViewer);

ReactDOM.render(<News category={article.getAttribute('category')} />,
  article);

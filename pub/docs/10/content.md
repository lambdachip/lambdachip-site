
Usually, some of the people fear of upgrading the firmware since they are araid of bricking the device.
Fortunately, you will never brick LambdaChip hardware when you upgrade/downgrade its firmware. We hope you enjoy
the process since it's safe and easy.

## Before upgrade the firmware

Please remember there's certain relationship of version number between the Laco compiler and LambdaChip VM firmware.

The version number is a triple set: **major.middle.minor**, for example, **v0.0.1**, the major is 0, middle is 0 and the minor is 1.
Not so hard.

You should remember that we will increase major or middle when there's incompatible changes with the previous version.
If your Laco compiler is **v0.0.5**, then it's only compatible with the **v0.0.x** LambdaChip VM firmware, **x** is an arbitrary positive integer;
**v0.2.6** compiler is compatible with **v0.2.x** firmware; **v1.3.5** compiler is compatible with **v1.3.x** firmware, and so on.

*NOTE: If you find your compiler is incompatible with your firmware, you may consider to change Laco compiler docker image to a
different version, or downgrade/upgrade your firmware. The downgrade process is exactly the same with the upgrade process.*

## Upgrade LambdaChip VM firmware

We assume you have downloaded the new firmware from [download page](/articles/misc/download). You will get
**firmware.bin** after you unzip the downloaded file. **Don't change its name**. The bootloader will check
the file named **firmware.bin** in the TF card, then flash it automatically.

So you will put **firmware.bin** to your TF card, and insert it to LambdaChip hardware, for an instance, your Alonzo board.
Then power-on, you will see the LED red light and blink.
<center>
<img src="/img/red_led.jpg" title="Red LED" alt="Red LED" width="450"/>
</center>

Please don't power-off before you see the LED green light.

<center>
<img src="/img/green_led.jpg" title="Green LED" alt="Green LED" width="450"/>
</center>

What if you cut off the power before you see the green light? Just repeat the upgrade process again. As we said, it's safe.

*NOTE: The firmware.bin would be removed if the upgrade has finished.*

*NOTE: If there's a program.lef file on your TF card, it'll run automatically after you see LED green light.*

## Upgrade the bootloader

**Warning: Upgrade bootloader is for advanced developer. You should have some experiences to work under GNU/Linux.**

Most of the time, you don't have to upgrade bootloader. But there are two situations you may consider to flash bootloader:
- You can't upgrade LambdaChip VM firmware successfully. This maybe because the bootloader was broken for some reasons.
- You want to install an enhanced bootloader. Then you're on your own risk to do so.

### The preparation

*NOTE: All the necessary parts are included in the complete version pack.*

You must have Saruman debugger to flash bootloader. The Saruman debugger is only provided in complete version, if you bought standard version,
then you may have to borrow one. Anyway, every serious LambdaChip maker should have at least one complete version.

<center>
<img src="/img/saruman_coin.png" title="Saruman debugger" alt="Saruman debugger" width="800"/>
</center>

A 10-pin ribbon that connects Saruman to LambdaChip hardware, say, Alonzo board.

<center>
<img src="/img/10-pin-ribbon.jpg" title="10-pin ribbon" alt="10-pin ribbon" width="450"/>
</center>

A Type-C cable to power the LambdaChip hardware.

<center>
<img src="/img/t-cable.jpg" title="TypeC-2-TypeC cable" alt="TypeC-2-TypeC" width="450"/>
</center>

### Flash the bootloader

First, please connect your Saruman debugger and LambdaChip hardware like this picture:

<center>
<img src="/img/debug_conn.jpg" title="Debug connect" alt="Debug connect" width="450"/>
</center>

**NOTE: Don't forget to connect the LambdaChip board (Alonzo board) to the power with Type-C cable, otherwise there's no device will be detected.**

Then insert Saruman to the USB slot of your PC.

Now you should see **ttyACM0** or **ttyACM1** device on you PC. If there are more serial ports, the index will increase accordingly.
```bash
ls /dev/ttyACM*
```
If you haven't installed **gdb-multiarch**, please install it. We assume you're using Debian or Ubuntu:
```bash
sudo apt install gdb-multiarch
```
You should be able to run gdb-multiarch now.
```bash
gdb-multiarch
(gdb) set debug arm
(gdb) target extended-remote /dev/ttyACM0
```
Let's say you've put the **bootloader.elf** in */dev/shm*:
```bash
(gdb) file /dev/shm/bootloader.elf
# Reading symbols from bootloader.elf...
(gdb) monitor swdp_scan
# Target voltage: 3.3V
# Available Targets:
# No. Att Driver
#  1      STM32F40x M4
(gdb) attach 1
# Attaching to program:  /dev/shm/zephyr.elf, Remote target
(gdb) load
# Loading section rom_start, size 0x198 lma 0x8000000
# Loading section text, size 0x7692 lma 0x80001c0
# Loading section .ARM.exidx, size 0x8 lma 0x8007854
# Loading section initlevel, size 0xb8 lma 0x800785c
# Loading section sw_isr_table, size 0x2b0 lma 0x8007914
# Loading section rodata, size 0x7fc lma 0x8007bc4
# Loading section datas, size 0xe0 lma 0x80083c0
# Loading section devices, size 0xe4 lma 0x80084a0
# Loading section k_mem_slab_area, size 0x38 lma 0x8008584
# Start address 0x08001f50, load size 34194
# Transfer rate: 35 KB/sec, 814 bytes/write.
```
Now disconnect the board, and try to upgrade a firmware to see if it works.

# End

## Before we start

Welcome to the world of LambdaChip!

LambdaChip is an idea composed of two ideas: functional programming and embedded systems.

### What is functional programming?
In nowadays' software industry, functional programming is a popular paradigm to reduce complexity with several methods that can be proved by mathematics. As a programming paradigm, users don't have to understand profound mathematical knowledge, they can just use the patterns and mechanisms to program. Many universities teach or have taught functional programming as part of their undergraduate Computer Science degrees. Some use it as their introduction to programming. Outside of computer science, functional programming is being used as a method to teach problem solving, algebra and geometric concepts. It has also been used as a tool to teach classical mechanics in Structure and Interpretation of Classical Mechanics in [Structure and Interpretation of Classical Mechanics](https://groups.csail.mit.edu/mac/users/gjs/6946/sicm-html/).

<center>
<img src="/img/fp.png" alt="The features of functional programming" title="The features of functional programming" width="650"/>
</center>

*The pic is referenced from the Internet, the author is unknown.*

### What are embedded systems?

Generally speaking, an embedded system is a computer around us that doesn't look like a computer. For example, your coffee machine, or a Laser printer.

In the Internet of Things (IoT) era, embedded systems are everywhere, they can be cameras, sensors, thermometers, etc. Obviously, embedded software development is the base of IoT. Furthermore, IoT has changed embedded development. Because IoT requires more complex processing of the data acquired by the embedded system, on a higher level, cloud computing and machine learning was introduced.

<center>
<img src="/img/embedded_system.jpg" alt="The application of embedded systems" title="The application of embedded systems" width="650"/>
</center>

*The pic is referenced from the Internet, the author is unknown.*

## What is LambdaChip?

LambdaChip is a functional programming featured Virtual Machine which runs on an embedded system. Usually, embedded systems have extremely limited resources, for instance, 50KB RAMs and less than 80MHz CPU. LambdaChip aims to provide fair speed on such a compact system.

That is to say, you never use C/C++ to develop embedded software with LambdaChip, you will learn and use Scheme which is a famous multi-paradigm programming language. Scheme is a dialect of Lisp and widely used for functional programming research and teaching.

<center>
<img src="/img/scheme_is_better.jpg" alt="The benefit of Scheme" title="The benefit of Scheme" width="800"/>
</center>

*This conclusion is referenced from Erann Gat's paper [Point of view: Lisp as an alternative to Java](https://dl.acm.org/doi/10.1145/355137.355142).*

*The pic is referenced from [LambdaNative](https://www.lambdanative.org/) which is another cool project to write Android apps with Scheme.*

LambdaChip implements an optimized instruction set to run LEF (LambdaChip Executable Format) bytecode on embedded systems. Its compiler is named Laco which means Lambda Compiler. Laco is not an interpreter, but an optimized compiler that can optimize the code to generate compact and efficient bytecode.

You may read the [Laco compiler introduction](/articles/docs/2) for more details.

## Is LambdaChip open-sourced?

LambdaChip is not only open-sourced, moreover, it's free licensed. Compared to the open-source model, the free license will grant you four freedoms:
- Free to run
- Free to study
- Free to redistribute
- Free to improve or modify

Obviously, these four freedoms surpass open-source definitely.

The Laco compiler is licensed with GPLv3, and the firmware [LambdaChip Virtual Machine](/articles/docs/3) is licensed with LGPLv3. The hardware design of LambdaChip Alonzo board is free hardware design which is licensed with Creative Commons 4.0, CC-BY-SA.

## What's next?

If you're interested in LambdaChip, we recommend you learn about our first product,  [the Alonzo board](/articles/news/0).

If you already have an Alonzo board, please read [the Alonzo board quick start](/articles/docs/1).

You are welcome to read our [news about the product](/articles/news).

Please read our [technical documents](/articles/docs) to learn deeper things.

# End

**NOTE: This is for advanced users.**

In this article, we introduce the process to be a contributor of LambdaChip.

## How to be a common contributor
The common contributor focuses on the current LambdaChip implementation. So one may use our docker environment to compile the current LambdaChip code.

### Prepare the docker environment
We strongly recommend GNU/Linux operating system. Of course, you may use Windows or macOS as well. The development environment of LambdaChip is system-independent.

Before continue, please install Docker, we recommend the [official installation of Docker](https://docs.docker.com/get-docker/).

### Get docker image of lambdachip-zephyr

You need to pull the docker environment for lambdachip-zephyr:
```bash
docker pull registry.gitlab.com/lambdachip/lambdachip-zephyr:latest
```
Please be patient since the image is huge.

```bash
cd lambdachip-zephyr
docker run -it --rm \
                 -v $PWD:/workspace \
                 -w /workspace \
                 -u "lambdachip:lambdachip" \
                 registry.gitlab.com/lambdachip/lambdachip-zephyr:latest \
                 bash
```
You should see something similar like this:

```bash
lambdachip@75c89755d8cc:/workspace$
```

Now you can modify the code with your preferred editor or IDE. When you finish it, just run make:
```bash
make
```

### Use GitLab

We use [GitLab](https://gitlab.com/lambdachip) to manage source code. And we recommend the book [Pro Git](https://git-scm.com/book/en/v2).

## How to be a core contributor
A core contributor needs to work with the upstream ZephyrRTOS source code.

Besides, a core contributor must use GNU/Linux.

### System requirements
GNU/Linux environment.
A TF card with FAT32 filesystem.

### Setting up Zephyr

[Getting start with Zephyr RTOS](https://docs.zephyrproject.org/latest/getting_started/index.html)
```bash
west init ~/zephyrproject
cd ~/zephyrproject
# after setting up zephyr environment, these executables will be available
dtc
ninja
arm-none-eabi-gcc
arm-none-eabi-ld
west
```


### Development toolchains
* Debian GNU/Linux based distributions

*guile-3.0 won't work for now, since the data structure of record has changed*
*We'll fix them later.*
guile-2.2

```bash
sudo apt-get update
sudo apt-get install binutils-arm-none-eabi gcc-arm-none-eabi guile-2.2
sudo apt-get install libnewlib-arm-none-eabi libnewlib-dev
sudo apt-get install libstdc++-arm-none-eabi-newlib gdb-multiarch

arm-none-eabi-gcc --version
# My gcc arm version:
# arm-none-eabi-gcc (15:8-2019-q3-1+b1) 8.3.1 20190703 (release) [gcc-8-branch revision 273027]
```

### LambdaChip environment

several projects are required

[LambdaChip Bootloader](https://gitlab.com/lambdachip/lambdachip-bootloader-zephyr.git)

[LambdaChip on Zephyr RTOS](https://gitlab.com/lambdachip/lambdachip-zephyr.git)

```bash
cd ~
git clone https://gitlab.com/lambdachip/lambdachip-zephyr.git
git clone https://gitlab.com/lambdachip/lambdachip-bootloader-zephyr.git
cd ~/zephyrproject/zephyr/boards/arm
# create a soft link for the board description files.
ln -s ~/lambdachip-zephyr/lambdachip_alonzo ./lambdachip_alonzo

# build bootloader and project
mkdir -p ~/zephyrproject/build
west build -b lambdachip_alonzo -s ~/lambdachip-zephyr -d ~/zephyrproject/build/lambdachip-zephyr
west build -b lambdachip_alonzo -s ~/lambdachip-bootloader-zephyr -d ~/zephyrproject/build/lambdachip-bootloader-zephyr
```

### Burn firmware
When you get the PCB, it should have a bootloader on it. The bootloader is able to read the firmware fron Tf card and burn the firmware into MCU.
If the bootloader is on the MCU. Skip this step
```bash
# Burn the firmware with Saruman Debugger

ls /dev/ttyACM*
# /dev/ttyACM0  /dev/ttyACM1 should show up, if there are more serial ports, the index will increase accordingly.
gdb-multiarch
(gdb) set debug arm
(gdb) target extended-remote /dev/ttyACM0
# (gdb) ~/zephyrproject/build/lambdachip-bootloader-zephyr/zephyr/zephyr.elf
(gdb) file ~/zephyrproject/build/lambdachip-bootloader-zephyr/zephyr/zephyr.elf
# Reading symbols from ~/zephyrproject/build/lambdachip-bootloader-zephyr/zephyr/zephyr.elf...
(gdb) monitor swdp_scan # monitor jtag_scan
# Target voltage: 3.3V
# Available Targets:
# No. Att Driver
#  1      STM32F40x M4
(gdb) attach 1
# Attaching to program: ~/zephyrproject/build/lambdachip-bootloader-zephyr/zephyr/zephyr.elf, Remote target
(gdb) load
# Loading section rom_start, size 0x198 lma 0x8000000
# Loading section text, size 0x7692 lma 0x80001c0
# Loading section .ARM.exidx, size 0x8 lma 0x8007854
# Loading section initlevel, size 0xb8 lma 0x800785c
# Loading section sw_isr_table, size 0x2b0 lma 0x8007914
# Loading section rodata, size 0x7fc lma 0x8007bc4
# Loading section datas, size 0xe0 lma 0x80083c0
# Loading section devices, size 0xe4 lma 0x80084a0
# Loading section k_mem_slab_area, size 0x38 lma 0x8008584
# Start address 0x08001f50, load size 34194
# Transfer rate: 35 KB/sec, 814 bytes/write.
```
After these steps, the bootloader is already burnt into the MCU. Then copy the firmware to TF card
Copy the binary to TF card with the name *fimrware.bin*, only FAT32 format is allowed for the TF card.

```bash
cp ~/zephyrproject/build/lambdachip-zephyr/zephyr/zephyr.bin /media/r/TF_CARD/firmware.bin
sudo umount /media/r/TF_CARD
```
* Remaining Steps
    * Then unplug the TF card from your computer,
    * Unplug the USB Type-C cable
    * Plug the Type-C cable to LambdaChip Alonzo.
    * Wait for around 10 seconds. Then the upgrade will complete. The firmware.bin will be deleted.
    * If your serial port is linked to the computer, you can check the upgrade log

### Compile Scheme code and let them run on LambdaChip Alonzo
Install guile 2.2 or 3.0, depends on your Linux distro.
```bash
sudo apt-get install guile-2.2
git clone https://gitlab.com/lambdachip/laco.git
cd laco
./configure
make -j3
```

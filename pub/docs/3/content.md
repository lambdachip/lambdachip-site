LambdaChip Virtual Machine is the firmware designed for compact embedded systems.
It can be thought of as an application running on a Real-time operating system (RTOS). This makes LambdaChip VM easy to port to other platforms if the RTOS has
already supported the platform.
By default, LambdaChip supports [Zephyr RTOS](https://www.zephyrproject.org/). LambdaChip VM can run on GNU/Linux as well, usually, [lambdachip-linux project](https://gitlab.com/lambdachip/lambdachip-linux) is used for the test.

## The idea behind

LambdaChip was inspired by [PICOBIT: A Compact Scheme System for Microcontrollers](http://www.iro.umontreal.ca/~feeley/papers/StAmourFeeleyIFL09.pdf) although
the design and implementation of LambdaChip have been radically reworked. Its architecture design (include [Laco compiler](articles/docs/2)) was largely influenced
by [GNU Guile](https://www.gnu.org/software/guile/) and [GCC](https://gcc.gnu.org/). Both of them are designed to support multi-languages on a generic compiler
infrastructure. LambdaChip aims to be a platform to support multiple [functional programming featured languages](https://en.wikipedia.org/wiki/Functional_programming)
that is highly optimized for embedded systems.

The first language on LambdaChip is [Scheme programming language](https://en.wikipedia.org/wiki/Scheme_(programming_language)). The scheme is a well-researched language that widely-used in computer science education and academic research for more than three decades. There are reasons that we choose Scheme to be the first language on
LambdaChip:
- It's easy to learn, [many universities use it for teaching CS basics](https://mitpress.mit.edu/sites/default/files/sicp/adopt-list.html)
- There're tons of tutorials and articles on the Internet
- It can help you to dig extremely deep into computer science if you want
- The knowledge you learned from Scheme can adapt to other programming languages
- The powerful expressiveness of the Scheme can help you solve the problem in a rapid way

Last but not the least, [Laco compiler](articles/docs/2) is also written in Scheme, so users may learn more things when they are interested in compiler theory.

## The basic theory

LambdaChip implements a [process virtual machine](https://en.wikipedia.org/wiki/Virtual_machine#Process_virtual_machines) running on RTOS.
A process virtual machine is an abstract of the real CPU. Its purpose is to provide a platform-independent programming environment that abstracts away details of
the underlying hardware or operating system and allows a program to execute in the same way on any platform.

A process virtual machine interprets a series of sequential bytecode.
Let's say we have a hello world program named **my.scm**:
```scheme
;; cat my.scm
(display "hello world\n")
```
In LambdaChip, the executable bytecode program in its assembly form may look like this:
```scheme
;; laco -t sasm my.scm
(lef
  (memory
   (gen-intern-symbol-table) ;
   (main-entry) ;
   ) ; Memory end

  (program
    (label ____principio) ; Proc `____principio' begin
     (push-string-object "hello world\n") ;
     (prim-call 6 #t) ; Call primitive `display'
    (label-end ____principio); Proc `____principio' end
  (halt)
  ) ; Program end

 (clean
  ) ; Clean end

) ; End LEF
```
And its binary form may look like this:
```bash
# laco my.scm
# hexdump -C my.lef
00000000  4c 45 46 00 00 01 00 00  00 08 00 00 00 11 00 00  |LEF.............|
00000010  00 00 00 00 00 00 00 00  00 00 e2 08 68 65 6c 6c  |............hell|
00000020  6f 20 77 6f 72 6c 64 0a  00 c6 ff                 |o world....|
0000002b
```
In LambdaChip VM, there's a big while-loop to execute each instruction one by one, till **(halt)**.

## The benefit

LambdaChip VM is designed for embedded systems. The size of the above *hello world* bytecode program is only **43 bytes**.
Comparing to other interpreters that run on the embedded system, LambdaChip provides an optimized compiler and VM for a compact system.
It supports [proper tail-call optimization (TCO)](https://en.wikipedia.org/wiki/Tail_call) according to the Scheme standard,
and it has good optimization for [Closures](https://en.wikipedia.org/wiki/Closure_(computer_programming)).
Besides, there are dedicated primitive APIs to control peripherals, and an efficient [Garbage Collector (GC)](https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)).

## Compile LambdaChip firmware

**NOTE: This is only for advanced developers. You should have a GNU/Linux environment.**

Before continue, please install Docker, we recommend the [official installation of Docker](https://docs.docker.com/get-docker/).

First, clone the repository:
```bash
git clone --recursive https://gitlab.com/lambdachip/lambdachip-zephyr.git
```

You need to pull the docker environment for lambdachip-zephyr:
```bash
docker pull registry.gitlab.com/lambdachip/lambdachip-zephyr:latest
```
Please be patient since the image is huge.
```bash
cd lambdachip-zephyr
docker run -it --rm \
                 -v $PWD:/workspace \
                 -w /workspace \
                 -u "lambdachip:lambdachip" \
                 registry.gitlab.com/lambdachip/lambdachip-zephyr:latest \
                 bash
```
You should see something similar like this:

```bash
lambdachip@75c89755d8cc:/workspace$
```
Now just run make:
```bash
make
```

## Flash the firmware

Please rename the firmware to **firmware.bin**:
```bash
cp build/zephyr/zephyr.bin firmware.bin
```
*NOTE: Now you should press ctrl+d to exit the docker environment!*

Please read [bootloader & firmware upgrade guide](https://lambdachip.com/articles/docs/10/) to learn how to flash the firmware.

## End

<title>LambdaChip - The future of IoT</title>
<@icon favicon.png %>
<meta
    name="viewport"
    content="minimum-scale=1, initial-scale=1, width=device-width"
/>
<meta name="description" content="LambdaChip is an IoT solution provider in Shenzhen. We Develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware of IoT solution. ">
<meta name="keywords" content="IoT, solution, solution provider, IoT solution provider, embedded, embedded system, free software, opensource, open source, FOSS, hardware, Scheme, Lua, Python, Toolchain, functional programming">
<meta property="og:site_name" content="LambdaChip - The future of IoT">
<meta property="og:url" content="https://lambdachip.com/">
<meta property="og:title" content="LambdaChip - The future of IoT">
<meta property="og:type" content="website">
<meta property="og:description" content="LambdaChip is an IoT solution provider in Shenzhen. We Develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware of IoT solution.">

<meta property="og:image" content="http://cdn.shopifycdn.net/s/files/1/0562/4545/2980/files/banner_f1447d90-3d08-4eac-ae01-902ffda06cc0_1200x1200.png?v=1619856175">
<meta property="og:image:secure_url" content="https://cdn.shopifycdn.net/s/files/1/0562/4545/2980/files/banner_f1447d90-3d08-4eac-ae01-902ffda06cc0_1200x1200.png?v=1619856175">


<meta name="twitter:site" content="@lambdachip">

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="LambdaChip - The future of IoT">
<meta name="twitter:description" content="The advanced software and hardware provider of Asia. Develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware.">
<meta charset="utf-8" />
<@js runtime.js %>
<@js react.js %>
<@js material-ui.js %>
<@js vendors.js %>
<!-- <script src="/node_modules/react/umd/react.development.min.js"></script>
     <script src="/node_modules/react-dom/umd/react-dom.development.min.js"></script> -->

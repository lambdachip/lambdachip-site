## Software

### Compiler
[Laco compiler](https://gitlab.com/lambdachip/laco).

### Firmeware
[LambdaChip core](https://gitlab.com/lambdachip/lambdachip).

#### Linux
[Linux port](https://gitlab.com/lambdachip/lambdachip-linux).

#### ZephyrRTOS
[ZephyrRTOS port](https://gitlab.com/lambdachip/lambdachip-zephyr).

[ZephyrRTOS bootloader](https://gitlab.com/lambdachip/lambdachip-bootloader-zephyr).

[ZephyrRTOS BSP](https://gitlab.com/lambdachip/lambdachip-zephyr-bsp).

## Hardware

[Alonzo board schematics](https://gitlab.com/lambdachip/alonzo_schematic).

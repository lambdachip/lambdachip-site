
You may need to read [bootloader & firmware upgrade guide](/articles/docs/10/).

## Download LambdaChip VM firmware

[LambadChip Firmware v0.4.4](https://gitlab.com/lambdachip/firmware/-/archive/v0.4.4/firmware-v0.4.4.zip)

[Previous version download](https://gitlab.com/lambdachip/firmware/-/tags)

## Download LambdaChip bootloader

**NOTE: Upgrade bootloader requires Saruman debugger which is only provided in complete version.**

[Bootloader v0.0.1](https://gitlab.com/lambdachip/bootloader-blob/-/archive/v0.0.1/bootloader-blob-v0.0.1.zip)

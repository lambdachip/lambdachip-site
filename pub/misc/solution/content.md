We provide IoT (Internet of Things) solutions and consulting services.

[LambdaChip Solution intro (EN)](/misc/solution/LambdaChip_solution_EN.pdf)

[LambdaChip Solution intro (中文)](/misc/solution/LambdaChip_solution_CN.pdf)

# Contact
Contact us via business@lambdachip.com


# End

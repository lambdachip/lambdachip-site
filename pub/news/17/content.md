<center>
<img src="/img/v0.3.3.png" title="LambdaChip v0.3.3 released" alt="LambdaChip v0.3.3 released"/>
</center>

## Introduction

LambdaChip is the modern advanced platform for embedded development. You can use functional programming paradigm to control hardware, make cool stuffs, or IoT prototyping.

The software of LambdaChip is FOSS, and its hardware is certified open hardware. Free as in freedom, not free beer.

## Changelog

v0.3.3 was a bugfix release. So there's no any new features.

### Fix numeric tower

Scheme supports a complete numeric tower, which contains integer, real, complex, fraction, exact number and inexact number. We've fixed many bugs in numeric tower and added many tests.

### Fix to detect integer correctly

Scheme separates exact and inexact numbers. Some other dynamic languages always use double float to represent a number. But in Scheme, a exact integer is represented as an integer.

### Fix to ensure applicative-order

Scheme language is applicative-order, which means the expression will be evaluated before it's applied. There was a bug in a few cases not ensure applicative-order, so that some computation will be run more than once. Now it's fixed.

### Fix the uniqueness of symbol object generation

The symbols should be unique globally, this is the best part of the symbols. There was a bug that older version will generate more than one object when the symbol appears more than once. Now it's fixed.

### Fix collection object size

The collection object (List, Vector, etc) didn't support zero size which caused trouble. Now it's fixed.

### Fix GC to correctly collect repeated object in a List

### Fix list-ref

### Fix named-let issue in a closure

### Fix Let-binding in closure conversion

### Fix to capture free-variables correctly in a closure

### Fix misc bugs in closure-conversion

### Fix to count correct arity of a closure

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco:latest
```
or
```bash
docker pull registry.gitlab.com/lambdachip/laco:0.3.3
```
## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## About us
Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is a globally-oriented technical comapny that develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware. Our aim is to provide good quality FOSS, and reduce the complexity in the development.

## Go to LambdaChip Shop!

Support FOSS and open hardware, join us!

[Buy LambdaChip Alonzo board](https://shop.lambdachip.com), you don't have to sign-up an account to order.

## End

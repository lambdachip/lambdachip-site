<center>
<img src="/img/v0.3.1.png" title="LambdaChip v0.3.1 released" alt="LambdaChip v0.3.1 released"/>
</center>

## Introduction

LambdaChip is developped by Tuwei Technology. LambdaChip is the Functional Programming Virtual Machine optimized for embedded system. It's 100% free software and free hardware design.

The Laco compiler is GPLv3, and the LambdaChip VM firmware is LGPLv3, the hardware design is CC-BY-SA.

[Learn more about LambdaChip](/articles/docs/0).

[See what you like in LambdaChip Shop](https://shop.lambdachip.com).

## Changelog

v0.3.1 was a bugfix release. So there's no any new features.

### Fix Named-Let expression
Named-let expression is a common way to write a tail-recursive call instead of of loop. There was a bug when you want to run tail-call infinitely, not it's fixed.

```scheme
(let lp((n 1))
  (display n)
  (newline)
  (if (= n 100)
      #t
      (lp (+ 1 n))))
```

### Fix List with variables
There was a bug when you put variables in a list, now it's fixed.
```scheme
(define a 1)
(define b 2)
(display (list a b))
```

### Fix to call closure recursively
This was related to named-let issue, see above.

### Fix local definition
When you defined variables in let expression, it should be renamed before it's converted to CPS.

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco:latest
```
or
```bash
docker pull registry.gitlab.com/lambdachip/laco:0.3.1
```
## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## About us
Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is a globally-oriented technical comapny that develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware. Our aim is to provide good quality FOSS, and reduce the complexity in the development.

## Buy LambdaChip hardware to support us!

LambdaChip hardware is our product to run LambdaChip software for rapid IoT prototyping. You can enjoy the convenience of LambdaChip with its dedicated hardware.

[See what you like in LambdaChip Shop](https://shop.lambdachip.com).

## End

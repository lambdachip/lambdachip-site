<center>
<img src="/img/v0.3.2.png" title="LambdaChip v0.3.2 released" alt="LambdaChip v0.3.2 released"/>
</center>

## Introduction

LambdaChip is the modern advanced platform for embedded development. You can use functional programming paradigm to control hardware, make cool stuffs, or IoT prototyping.

LambdaChip firmware is FOSS, and the Alonzo board is free hardware design. Free as in freedom, not free beer.

## Changelog

v0.3.2 was a bugfix release. So there's no any new features.

### Fix BLE debug module
The LambdaChip firmware provides a way to debug your embedded program via BLE (Bluetooth Low Energy) rather than serial port in traditional way. So you can debug the embedded program with your smart phone wirelessly. Users just need to put **(ble-enable!)** in front of the body of the program.

There was a bug in **ble-enable!** that it will reset the BLE each time when you run the program. This will cause problem to output nothing.

```scheme
(ble-enable!)
```

### Fix symbol object
There was a inconsistent bug in symbol object implementation that may cause segfault when you print it.

### Fix let* binding when there's side-effects
If you bind variables but you don't use them later, these variables will be eliminated in the optimization. However, if there's side-effects in the bound expression, it should be kept. Now it's fixed.

```scheme
(define (test)
  (let* ((x (display "xxx\n"))
         (y (display "yyy\n")))
    (display "Optimized with side-effects!\n")))

(test)
```
In the above case, the output result should be:
```
xxx
yyy
Optimized with side-effects!
```

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco:latest
```
or
```bash
docker pull registry.gitlab.com/lambdachip/laco:0.3.2
```
## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## About us
Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is a globally-oriented technical comapny that develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware. Our aim is to provide good quality FOSS, and reduce the complexity in the development.

## Buy LambdaChip hardware to support us!

[Learn more about LambdaChip](/articles/docs/0).

[Buy LambdaChip Alonzo board](https://shop.lambdachip.com), you don't have to sign-up an account to order.

## End

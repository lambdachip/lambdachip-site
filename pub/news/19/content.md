<center>
<img src="/img/release.png" title="LambdaChip v0.4.0 released" alt="LambdaChip v0.4.0 released"/>
</center>

## Introduction

LambdaChip is the modern advanced platform for embedded development. You can use functional programming paradigm to control hardware, make cool stuffs, or IoT prototyping.

The software of LambdaChip is FOSS, and its hardware is certified open hardware. Free as in freedom, not free beer.

## Changelog

### Add R7RS exception handling

In Scheme 7th standard (R7RS), there're new exception primitivies:
- with-exception-handler
- raise
- raise-continuable

The essential primitive is **with-exception-handler**:
```scheme
(with-exception-handler
   exception-handler
   thunk)
 ```
As you may know, a thunk is a nullary function, I'll give you an example to show the power of **raise-continuable**:
```scheme
(with-exception-handler
    (lambda (e)
      (cond
       ((eq? e 'need-number)
        (display "catch! Let me return a number for you!\n")
        5)
       (else "no")))
  (lambda ()
    (display "1\n")
    (display (+ (raise-continuable 'need-number) 10))
    (display "\n2\n")))
```
Guess the result.

The continuable exception raised by **raise-continuable** will first pass the argumensts into the exception-handler, and return the result of the exception-handler
to where it was broken to continue the work, and run like nothing happened. So the result is:
```md
1
catch! Let me return a number for you!
15
2
```
What if you use **raise** instead of **raise-continuable**?
It'll do the similar, but it won't return the result to the break point to continue the work. It throws an error to exit the program:
```md
1
catch! Let me return a number for you!
Exception occurred with non-continuable object: need-number
```
Fortunately, the implementation of **with-exception-handler** in LambdaChip is lightweight and fast, we don't actually capture the continuation by copying stack data.
LambdaChip will backup related cursors (program counter and stack pointer) and restore them to continue the broken work.

We hope this exception handling mechanisim can help to make embedded program more stable and efficient.

### Enhance GC, and fix tons of bugs

Since 0.4.0, we added two configurable GC parameters:
- PRE_ARN (Pre-allocated Active Root tree Node), if you're not familiar with *active root*, you may call it *active record* as well.
- PRE_OLN (Pre-allocated Object List Node)

As you may know, the GC of LambdaChip is a simplified generational and object-classified tracing GC that uses a mark-and-sweep algorithm.
However, in LambdaChip, we use the terms collect-and-sweep, since the first step may not be just immutably tagging sometimes.


The ARN is used to hold the object location of all the active records in the stack.
If you're a newbie of functional programming, try to imagine that the object location is the pointer address of the variable. Most of the time it is.
The object that appears in ARN will be protected from the GC recycling process. The Active Root Tree will be refreshed quickly each time when the GC is triggered.
We implement the Active Root Tree with an RB-tree. Alternatively, people may call it a reference tree as well.

The OLN is used to hold the allocated objects in the memory pool. If the object was freed, OLN would be recycled as well.
In other words, OLN indicates the number of objects that you can allocate in the system. If it's not enough for a new allocation, then it triggers GC.

The pre-allocation is necessary since GC will be in a dead loop when we have no memory at all.
We have to make sure to pre-allocate some RAMs to GC for its work.

- If ARN is too small, you can't have many local objects in the stack frame.
- If ARN is too large, you just waste RAMs.

- If OLN is too small, your program may trigger GC frequently. However, a smaller OLN is helpful when you have big objects, for example, large lists.
- If OLN is too large, the program running without any GC, depends on your algorithm. However, if it triggers GC, then you may have a slow GC process.
The default value:
- ARN is 100
- OLN is 100

We'll post a complete document of GC later.

### Add a SPI API

- spi-tranceive!

### Add more I2C APIs

- i2c-read-list!
- i2c-write-list!


### Add missing preds

-  list?
-  string?
-  char?
-  keyword?
-  symbol?
-  procedure?
-  primitive?
-  boolean?
-  number?
-  integer?
-  real?
-  rational?
-  complex?


### Fix named-let in definition bug

See [https://gitlab.com/lambdachip/laco/-/issues/5](https://gitlab.com/lambdachip/laco/-/issues/5).

### Fix multi-bindings in named-let definition

See [https://gitlab.com/lambdachip/laco/-/issues/4](https://gitlab.com/lambdachip/laco/-/issues/4).

### Fix bug in nested lambda

See [https://gitlab.com/lambdachip/laco/-/issues/6](https://gitlab.com/lambdachip/laco/-/issues/6).

### Fix to put expressions between definitions

For example:
```scheme
(define a 123)
(display a)
(define b 321)
```

### Fix to convert re-definition to assignment correctly

For example:
```scheme
(define a 123)
(display a)
(define a 321)
(display a)
```
The result should be 123321. The second definition of **a** will be converted to an assignment inexplicitly.

### Fix delta-reduction to work with preds

If any possible, delta-reduction will evaluate the code safely in the compile time for type checking predicates.

For example:
```scheme
(display (pair? 123))
```
In compile time, this code will be optimized to:
```scheme
(display #f)
```
This is suitable for any predicates, say, list? string? number? ... etc.

### Fix global list that nests a constant

### Fix eq?

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco:latest
```
or
```bash
docker pull registry.gitlab.com/lambdachip/laco:0.4.0
```
## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## About us
Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is a globally-oriented technical comapny that develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware. Our aim is to provide good quality FOSS, and reduce the complexity in the development.

## Go to LambdaChip Shop!

Support FOSS and open hardware, join us!

[Buy LambdaChip Alonzo board](https://shop.lambdachip.com), you don't have to sign-up an account to order.

## End

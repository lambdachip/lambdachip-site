<center>
<img src="/img/roys_view.png" title="Roy's View" alt="Roy's View"/>
</center>

**[Roy](https://nalaginrut.com/about) is the CEO of Tuwei Technology (图桅科技), who is also the original author of LambdaChip.
This article is referred to [a post on his blog](https://nalaginrut.com/archives/2021/05/04/scheme%20language%20is%20good%20enough%20for%20products).**


In the past decade, I've been trying to use Scheme programming language in the product environment. The existing lore around Scheme is always fascinating, paradoxical, and mystical. However, although Scheme is prominent in the academy, I'm pretty sure only a few people had tried it in a product. Because I didn't see many discussions about the problems using Scheme in product development. Most people were just following other people's opinions. And no one discusses the real problem, and how to solve the problem.
My choice is GNU Guile, and I have also used other Scheme implementations. Of course, my experience is limited, and it doesn't mean GNU Guile is mature enough to replace Python.  My opinions are not only for GNU Guile, they're considered for other Scheme implementations as well. I'd recommend you take a look at Chez Scheme, Racket, Chicken Scheme...to learn about the progress of Scheme community in the last two decades.
So this article is not going to tell you why to choose Scheme rather than Python. No, it's not the way to go. What we discuss is that if you already notice the trend of functional programming, and you want to take a look at some well-known functional programming languages, this article may help you to learn about the power of Scheme in a product.

## The cliché question: why bother to use Scheme?
There are tons of answers. I have to save time to list them exhaustively. If you are not familiar with Scheme land, here is what you should know.
- Scheme provides great expressiveness to write painless code to reduce the complexity.
- The low-level details are not the main concern in business logic, Scheme provides a good way for rapid development.
- Less complexity brings fewer bugs, but it doesn't mean there are no fatal bugs in Scheme code, it depends on the level of the programmer.
- Scheme is easy to understand. This is important when we have to analyze the program to debug.
- Scheme is a standardized language, the grammar and standard API are well defined.
- Full lambda calculus support. The lambda combination provides better expressiveness when you need it.
- From the perspective of professional software development, the Scheme is worth to invest. I'll prove it in this article.

## The first illusion: it's hard to find Scheme programmers
Well, yes, it's hard to hire a professional Scheme programmer directly. Not many people have experience using Scheme in the product.
Five years ago, when I first time tried to build a Scheme team, I was also concerned about this issue. However, 2 months later, I realized that it's never a problem. We can train a Scheme programmer at a low cost. So here is my short story about the recruit.
At that time, we use C++14 for system programming, and Scheme for business logic scripting. Now that I am concerned about the difficulty to hire a Scheme programmer, my idea was only to ask about the functional programming part in C++. Nowadays, C++ has many functional features, so C++ programmer has to know something about FP. It's a fashion, right?
If the candidates were good enough to get our offer, I would teach them SICP in the newbie training. Of course, SICP is a big book for newbies, so I only picked the first 2 chapters. The "data abstract" is for understanding object-oriented, and the "procedure abstract" concept is for polishing algorithm implementation. And we dropped "meta-language abstract" and "machine abstract" in the SICP since we didn't create a compiler business (we do it now, that's another story I will tell later).
The training had lasted one month. After training, they started to do the small works on the product, fix bugs or tiny features. SICP is not only a book but also a formalized educational system created by the profound mind. So we can take advantage of this system, finish the training very soon, and use what you have learned in the daily work when it's still warm in your brain.
The SICP educational system can help us to train good Scheme programmers in a short time. Any technology company should have a training period for newbies we can do it in one month. It's a good and affordable solution. We've kept using it this way for five years, so far so good.
Think about it, we spent zero money on this training, and we transfer the knowledge of SICP into a product in a short time.

## The second illusion: Scheme is not fast
Not true anymore, have you heard of Chez Scheme?
Many years ago, when I was still a post-graduate. I've ever fascinated by the speed of the programming language.
When I became a professional developer, I realized that there are at least 3 kinds of speed in programming:
1. Learning speed: how easy to learn a language with zero knowledge?
2. Development speed: can we reduce the coding work as possible?
3. Execution speed: how fast can it execute?
Knowing about this, I suddenly understand why Ruby on Rails is popular in web development, although it was considered a slow language.
If we only consider the execution speed, the assembly or even machine language would be a better choice.

## The third illusion: ecosystem is the most important for a language
My friend, please, don't trust this conjecture anymore. It's not true in real work.
If you want a true story, here it is: the language is the most important for an ecosystem.
Python ecosystem is only for the Python language. Python has a big ecosystem for historical reasons, but it doesn't mean other languages need a comparable big ecosystem.
There's only one true ecosystem, the C ecosystem. The dynamic language modules are just the bindings of the existing C libraries.
So if a language has good FFI (Foreign Function Interface) support, it can take advantage of all C libraries. Today, people may concern more about distributed processing. The distributed system can be heterogeneous so that we don't have to build everything in just one ecosystem.
On the other side, a big ecosystem may contain a sufficient number of packages, but it doesn't mean all these packages and their dependencies are of good quality and maintenance. We shouldn't have any blind faith in it.
So let me break this illusion, an ecosystem may be significant in some situations, but not the most important for a language. When we create a language, we only consider its paradigm, grammar, expressiveness, and optimizing penalties.  These things are the most important for a programming language.
In the past, a programming language was created and driven by a company. People have no reason to contribute to its ecosystem without any payment. So for a commercialized programming language community, the ecosystem was important, otherwise, it has grown slowly, and the investment was wasted. Nowadays, the programming language is mainly driven by FOSS (free-and-opensource software) community. The ecosystem is naturally growing under FOSS power. An experienced developer doesn't worry about the expressiveness of a language since it decides your coding efficiency.

## Cons 1: Scheme is not ready for deep learning, yet
Today, deep learning is hot. I'd like to introduce the [AISCM project](https://wedesoft.github.io/aiscm/) which is written in GNU Guile using LLVM JIT and based on TensorFlow. It works. It's cool, and I had some patches to it. However, I don't have much time to spend on it. So if you hope to use Scheme for deep learning seriously, I'd recommend you contribute to AISCM.
Warning, there's already a middle codebase in AISCM. And it contains the support of OpenCV, FFmpeg as well. Thanks to the author Jan Wedekind.
In the other Scheme implementation, I have less information about their progress on deep learning things. But it's just the matter of my short-sightedness.

## Cons 2: Scheme has fragmented community
For historical reasons, there're many Scheme implementations, but the community is fragmented. Today, we see some hopes.
First, the Scheme standardization has good progress in the past decade. The R7RS has separated Scheme into small and big languages. IMHO, it's a good step. Because it keeps the minimalism of Scheme and provided a way to make Scheme an industrial level libraries spec.
My own business relies on [R7RS-small](https://small.r7rs.org/attachment/r7rs.pdf), which is just like created for our business. I'll talk about it later.
Another hope is the modern package manager.
For me, I choose GNU Guix, a functional package manager written with GNU Guile. Most of the GNU Guile libraries can install with GNU Guix. The package manager creates the ecosystem. In practice, we don't say "ecosystem" since it's a commercial concept. The GNU is an operating system community.
Of course, Racket and Chicken Scheme has a good package manager as well. This helps them to form a good community.
Anyway, the consistent standard spec and cool package manager may fix the fragmented community. But it needs time.

## Cons 3: Scheme is not pure
Functional programming folks may want to use pure functional programming language, which means there are no side effects.
Yes, the Scheme is not pure, and it's a multi-paradigm language, not only a functional programming language. But it provides full functional features that you can find in other pure languages. Although there are side-effects in Scheme, you can decide what and when to use it. I don't think it's a good idea to forbid people from the side effects. People should have the freedom to choose the way to use a language. The Scheme provides this freedom, this is maybe good or bad. But anyway, you are free to choose.

## Pros 1: Scheme is good for Web development
Nowadays, most of the GUI programs are replaced by SaaS. That's why I mentioned Web development rather than traditional GUI development.
In 2013, I started to write [GNU Artanis](https://artanis.dev), which a modern web framework in GNU Guile. At that time, I just want to do some simple cases to use the Scheme for the web.
I didn't expect GNU Artanis can help me to finish an industrial-level SOA (Service of Architecture) in logistics robotic system, but yes it did. And now, GNU Artanis is not a small piece of code anymore. It contains URL-remapping, relational mapping, HTML template, asynchronous co-routine server core, integration of modern Javascript frontend framework like React, etc. You can build your site or service in just one minute.
Racket and Chicken Scheme have a good chance for web development too. Before I started to write GNU Artanis, I've used Chicken Scheme for web programming.

## Pros 2: Scheme is worth to invest in a low cost
The Scheme has been well-researched for decades in the academy. From embedded systems to distributed server development, there're tons of Scheme papers to show positive outcomes.
So people will ask a question, for such a good language, how hard to learn and use it? The answer had been answered in the "first illusion" section:
In practice, we spent zero money on Scheme training, and there's a good educational system SICP that we can transfer the knowledge into a product in a short time.
The zero money story is true, and we didn't count my salary since I didn't spend much time training them myself. Of course, we will discuss the programming issue, but most of the discussions are around general programming, not Scheme specific. They can fluently write Scheme code by themselves if you've ever learned Scheme, you don't doubt it.
The cost is so low that it's harmless to give it a try.

## Pros 3: Scheme saves your coding time
As a seasoned C/C++ programmer, I have to say we shouldn't waste our time coding with C/C++ in the non-system-programming part. The business logic maybe changed according to the requirements. It's not flexible to write everything in C/C++. Don't waste your time on refactoring and debugging.
The Scheme has powerful expressiveness to help you reduce coding work.
But it depends on how you use it.
If you expect there're many libraries to save you work, Scheme is not the choice. Usually, this is for the proof of concept. You may choose Python.
If you use Scheme for scripting relies on your C/C++ codebase, it's a good way to go. The Scheme is minimalist and it's easy to be embedded as script code in a system.

## Pros 4: build your team with real CS knowledge
When you tried to complain about the bad coding in a code review again, have you realized that you never gave a chance to your team members to equip real CS knowledge for serious programming? The Scheme can bring such a chance, and it's easy to learn, finally, it's good enough to use in a product.

## So what's my business with Scheme now?
I'm glad that you finally ask me this question.
In 2020, I co-founded a company named Tuwei Technology (图桅科技) for rapid AIoT prototyping and solution. We developed LambdaChip, it is a virtual machine designed for embedded systems, which contains special supports for functional programming languages. The Laco compiler is an R7RS Scheme compiler. The compiler infrastructure is flexible enough to support multiple functional featured languages in the future.
In the past two decades, we can see great progress in modern software development: erupting FOSS communities, DevOps, SaaS, deep learning, functional programming, etc...we want to bring efficient development to embedded software development, and better workflow to integrate embedded software to AIoT solution.
Please [follow us on Twitter](https://twitter.com/lambdachip), join our [Reddit forum](https://www.reddit.com/r/lambdachip/), and bookmark [our site](https://lambdachip.com). We occasionally publish cool things.

Finally, choose your weapon wisely. And it's not bad to be diversity to learn more things.

## End

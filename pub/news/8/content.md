<center>
<img src="/img/v0.2.0.png" title="LambdaChip v0.2.0 released" alt="LambdaChip v0.2.0 released"/>
</center>

## Introduction

Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is a globally-oriented technical comapny that develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware.

LambdaChip is developped by Tuwei Technology. LambdaChip is the Functional Programming Virtual Machine optimized for embedded system. It's 100% free software and free hardware design.

The Laco compiler is GPLv3, and the LambdaChip VM firmware is LGPLv3, the hardware design is CC-BY-SA.

[Learn more about LambdaChip](/articles/docs/0).

[Buy LambdaChip hardware to support us](https://shop.lambdachip.com).

## Changelog

### Add tests for Continous Intergration (CI)

### Enhanced global operations

Now users can configure any pin on Alonzo Board as a GPIO easily.

```scheme
(define (loop x)
        (gpio-toggle! 'dev_gpio_pb3)
        (usleep 200000)
        (gpio-toggle! 'dev_gpio_pb10)
        (gpio-toggle! 'dev_led0)
        (usleep 200000)
        (display "x = ")
        (display x)
        (display "\n")
        (if (<= x 0)
            0
            (loop (- x 1))))

(define (main)
    (display "--------------start-----------\n")
    (device-configure! 'dev_led0)
    (device-configure! 'dev_gpio_pb10)
    (device-configure! 'dev_gpio_pb3)
    (loop 10))
  ```

### Misc bugfixes

- Re-fix dead-function-elimination for globals
- Fix to avoid tail-call optimizing (TCO) for non-recursive call

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco
```

## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## Buy LambdaChip hardware to support us!

[Buy LambdaChip hardware to support us](https://shop.lambdachip.com).

## End

It has been a while when most drivers are wrote in C/C++. But now, it is a fashion to write drivers in a more flexible way. Engineers doesn't have to take care of kinds of memory leaks, dangling pointers.
There are more drivers in dynamic programming languages.
We here provide a sensor driver by scheme.

<center>
<img src="/img/MMA8451Q_connection.jpg" alt="MMA8451Q connection" width="825"/>
</center>

The MMA8451Q is a smart, low-power, three-axis, capacitive, micromachined accelerometer with 14 bits of resolution.
The datasheet is here:

['https://www.nxp.com.cn/docs/en/data-sheet/MMA8451Q.pdf']('https://www.nxp.com.cn/docs/en/data-sheet/MMA8451Q.pdf')

Is it possible to write sensor drivers in scheme? How does scheme interoperate with lower level C drivers and hardware?
We implement this by modifying the compiler laco and the VM(virtual machine) lambdachip.

[https://www.gitlab.com/lambdachip/laco.git](https://www.gitlab.com/lambdachip/laco.git)

[https://gitlab.com/lambdachip/lambdachip.git](https://gitlab.com/lambdachip/lambdachip.git)

Here the user space code:

[https://gitlab.com/lambdachip/lambdachip-scheme-examples/-/blob/a61a8033ed80222f2023f9ed44b3245406da5ae9/mma8451q/mma8451q.scm](https://gitlab.com/lambdachip/lambdachip-scheme-examples/-/blob/a61a8033ed80222f2023f9ed44b3245406da5ae9/mma8451q/mma8451q.scm)

```scheme
;; global define for MMA8451 registers
(define MMA8451_DEFAULT_ADDRESS 29) ;; 0x1D
(define MMA8451_REG_WHOAMI 13) ;; 0x0D
(define MMA8451_REG_CTRL_REG1 42) ;; 0x2A

;; interpret 2 bytes of data as a signed 16 bit integer
(define (convert-to-int16 a b)
  (let ((c (+ (* 256 a) b)))
    (if (> c 32767)
        (- c 65536)
        c)))

;; read the MMA8451_REG_WHOAMI register and see the result
(define (mma8451_connection_check!)
  (let* ((ret (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_WHOAMI)))
    (if ret
        (if (= ret 26)
            #t
            #f)
        #f)))

;; write active bit in MMA8451 control register 1
(define (mma8451_set_state_active!)
  (i2c-write-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_CTRL_REG1 1)
  (let  ((ret (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_CTRL_REG1)))
    (if ret
        (if (= ret 1)
            #t
            #f)
        #f)))

;; read 6 registers, they represent the acceleration of x, y and z
(define (mma8451_read_all!)
  (let* ((reg1     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 1))
         (reg2     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 2))
         (reg3     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 3))
         (reg4     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 4))
         (reg5     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 5))
         (reg6     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 6)))
    (display "x = ")
    (display (convert-to-int16 reg1 reg2))
    (display ", y = ")
    (display (convert-to-int16 reg3 reg4))
    (display ", z = ")
    (display (convert-to-int16 reg5 reg6))
    (display "\n")))

;; read values then sleep 1/3 seconds
(define (loop x)
  (usleep 333333)
  (mma8451_read_all!)
  (if (<= x 0)
      0
      (loop (- x 1))))

;; check connection, activate the sensor, then read values periodically
(define (main)
  (ble-enable!)
  (if (mma8451_connection_check!)
      (begin (display "MMA8451Q connection OK!\n")
             (mma8451_set_state_active!)
             (loop 90))
      (display "Connection FAIL!\n"))
  (newline))


(main)
```

Here are the primitives we defined in the compiler:

[https://www.gitlab.com/lambdachip/laco.git](https://www.gitlab.com/lambdachip/laco.git)
```scheme
;; laco/laco/primitives.scm
(define-primitive (i2c-read-byte!)
  (lambda _
    (throw 'laco-error 'prim:get-board-id "BUG: i2c-read-byte! shouldn't be called in compile time!")))

(define-primitive (i2c-write-byte!)
  (lambda _
    (throw 'laco-error 'prim:get-board-id "BUG: i2c-write-byte! shouldn't be called in compile time!")))
```

This gets the parameters from scheme and send them into C code:

[https://gitlab.com/lambdachip/lambdachip.git](https://gitlab.com/lambdachip/lambdachip.git)
```C
    // lambdachip/vm.c
    switch (proc.attr.type)
    ...
    case prim_i2c_write_byte:
    case ...:
      {
        func_4_args_with_ret_t fn = (func_4_args_with_ret_t)prim->fn;
        // get the parameters from the stack
        Object o4 = POP_OBJ ();
        Object o3 = POP_OBJ ();
        Object o2 = POP_OBJ ();
        Object o1 = POP_OBJ ();
        // create an object for return
        Object ret = CREATE_RET_OBJ ();
        ret = *(fn (vm, &ret, &o1, &o2, &o3, &o4));
        PUSH_OBJ (ret);
        break;
      }
```

Here is the code that actually runs
```C
// lambdachip/primitives.c
static object_t _os_i2c_write_byte (vm_t vm, object_t ret, object_t dev,
                                    object_t dev_addr, object_t reg_addr,
                                    object_t value)
{
  // type checker
  VALIDATE (ret, imm_int);
  VALIDATE (dev, symbol);
  VALIDATE (dev_addr, imm_int);
  VALIDATE (reg_addr, imm_int);
  VALIDATE (value, imm_int);
  // get the device pointer from device tree
  super_device *p = translate_supper_dev_from_symbol (dev);
  uint8_t tx_buf[2] = {(imm_int_t)reg_addr->value, (imm_int_t)value->value};
  // call the i2c driver provided by zephyr OS
  int status
    = i2c_reg_write_byte (p->dev, (imm_int_t)dev_addr->value,
                          (imm_int_t)reg_addr->value, (imm_int_t)value->value);
  if (status != 0)
    ret = &GLOBAL_REF (false_const);
  else
    ret = &GLOBAL_REF (none_const);
  return ret;
}

```


With holding the sensor module and moving.
The result is showing here:
```
AT+AUTO+++=Y
MMA8451Q connection OK!
ret = 1
x = 4784, y = 15328, z = 4192
x = 4896, y = 15288, z = 4064
x = 3168, y = 13320, z = -2640
x = 768, y = 16352, z = 14984
x = 4320, y = 8872, z = 14208
x = 7296, y = -664, z = 8592
x = 2400, y = -6096, z = 11600
x = 5528, y = -12120, z = 6576
x = 7808, y = -9816, z = 5880
x = 10480, y = -4664, z = 9280
x = 15152, y = -3072, z = 6144
x = 4224, y = -9864, z = 7296
x = 4288, y = -8696, z = 12128
x = 912, y = -8968, z = -2776
x = 4400, y = 4008, z = 2472
x = 8680, y = 9048, z = 5048
x = 14480, y = 15264, z = -2176
x = 960, y = 4544, z = -1304
x = 3440, y = 14040, z = 5968
...
```

## What's next?

If you're interested in LambdaChip, we recommend you learn about [Alonzo board](/articles/news/0).

Take a look at [Peripheral API Guide](/articles/docs/11).

See what we have in [LambdaChip Shop](https://shop.lambdachip.com)

## End

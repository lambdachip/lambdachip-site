<center>
<img src="/img/v0.3.0.png" title="LambdaChip v0.3.0 released" alt="LambdaChip v0.3.0 released"/>
</center>

## Introduction

LambdaChip is developped by Tuwei Technology. LambdaChip is the Functional Programming Virtual Machine optimized for embedded system. It's 100% free software and free hardware design.

The Laco compiler is GPLv3, and the LambdaChip VM firmware is LGPLv3, the hardware design is CC-BY-SA.

[Learn more about LambdaChip](/articles/docs/0).

[Buy LambdaChip hardware to support us](https://shop.lambdachip.com).

## Changelog

### Support optional arguments
```scheme
(define* (sum x #:key (y 2) (z 3))
 (+ x y z))

(display (sum 1))
;; => 6
(sum 1 #:y 100 #:z 599)
;; => 700
```

### Add ble-reset!

### Add I2C API

I²C or I2C, pronounced I-squared-C, is a synchronous, multi-master, multi-slave, packet switched, single-ended, serial communication bus. It is widely used for attaching lower-speed peripheral ICs to processors and microcontrollers in short-distance, intra-board communication.

```scheme
(define (convert-to-int16 a b)
  (let ((c (+ (* 256 a) b)))
    (if (> c 32767)
        (- c 65536)
        c)))

(define (mma8451_connection_check!)
  (let* ((MMA8451_DEFAULT_ADDRESS 29) ;; 0x1D
         (MMA8451_REG_WHOAMI 13) ;; 0x0D
         (MMA8451_REG_CTRL_REG1 42) ;; 0x2A
         (ret     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_WHOAMI))
         )
    (if ret
        (if (= ret 26)
            #t
            #f)
        #f)))

(define (mma8451_set_state_active!)
  (let* ((MMA8451_DEFAULT_ADDRESS 29) ;; 0x1D
         (MMA8451_REG_WHOAMI 13) ;; 0x0D
         (MMA8451_REG_CTRL_REG1 42)) ;; 0x2A
    (i2c-write-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_CTRL_REG1 1)
    (let  ((ret     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS MMA8451_REG_CTRL_REG1)))
      (display "ret = ")
      (display ret)
      (newline)
      (if ret
          (if (= ret 1)
              #t
              #f)
          #f))))


(define (mma8451_read_all!)
  (let* ((MMA8451_DEFAULT_ADDRESS 29) ;; 0x1D
         (MMA8451_REG_WHOAMI 13) ;; 0x0D
         (MMA8451_REG_CTRL_REG1 42) ;; 0x2A
         ;; (reg0     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 0))
         (reg1     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 1))
         (reg2     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 2))
         (reg3     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 3))
         (reg4     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 4))
         (reg5     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 5))
         (reg6     (i2c-read-byte! 'dev_i2c2 MMA8451_DEFAULT_ADDRESS 6))
         )
    (display "x = ")
    (display (convert-to-int16 reg1 reg2))
    (display ", y = ")
    (display (convert-to-int16 reg3 reg4))
    (display ", z = ")
    (display (convert-to-int16 reg5 reg6))
    (display "\n")))

(define (loop x)
  (usleep 333333)
  (mma8451_read_all!)
  (if (<= x 0)
      0
      (loop (- x 1))))

(define (main)
  (ble-enable!)
  (if (mma8451_connection_check!)
      (begin (display "MMA8451Q connection OK!\n")
             (mma8451_set_state_active!)
             (loop 90))
      (display "Connection FAIL!\n"))
  (newline))

(main)
```
This dumb test may output like this:

<img src="/img/i2c-test.jpeg" title="I2C test" alt="I2C test"/>

### Add more I/O functions

- (read-char)
- (read-string size)
- (read-line)
- (list->string lst)

### Add effect-analysis

### Misc bugfixes

- Fix expression defined in a global
- Fix to return escaped closure correctly
- Fix to eliminate the unused functions correctly
- Fix append, car, cdr
- Fix pair object
- Fix global object index issue
- Fix fold-brach
- Fix let*

### Add more test cases

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco
```

## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## About us
Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is a globally-oriented technical comapny that develops, manufactures, supports, and sells FOSS (free-and-opensource software) and hardware. Our aim is to provide good quality FOSS, and reduce the complexity in the development.

## Buy LambdaChip hardware to support us!

LambdaChip hardware is our product to run LambdaChip software for rapid IoT prototyping. You can enjoy the convenience of LambdaChip with its dedicated hardware.

[Buy LambdaChip hardware to support us](https://shop.lambdachip.com).

## End

<center>
<img src="/img/fossasia_2021.png" title="Rafael in FOSS Asia 2021" alt="Rafael in FOSS Asia 2021"/>
</center>

What will happen when the embedded system meets Functional Programming? LambdaChip may answer.

LambdaChip is not only an open-source project, it's free licensed. LambdaChip VM firmware (LGPLv3), Laco compiler (GPLv3), hardware design (Creative Commons 4.0, CC-BY-SA).

[LambdaChip is a virtual machine](/articles/docs/3) designed for supporting functional featured languages on embedded systems, its [Laco compiler](/articles/docs/2) is designed to optimize the code for compact systems.

Embedded software development is the base of the Internet of Things (IoT). LambdaChip provides a solution for IoT prototyping rapidly.

LambdaChip is a good tool for CS students to learn functional programming, operating system, and compiler theory.

LambdaChip is also a powerful weapon for Makers to create cool stuff.

## What's next?

[Visit FOSS Asia 2021 Summit event](https://eventyay.com/e/fa96ae2c).

If you're interested in LambdaChip, we recommend you learn about our first product [Alonzo board](/articles/news/0).

If you already have an Alonzo board, please read [Alonzo board quick start](/articles/docs/1).

## End

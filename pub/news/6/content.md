<center>
<img src="/img/v0.1.0.png" title="LambdaChip v0.1.0 released" alt="LambdaChip v0.1.0 released"/>
</center>

## Introduction

LambdaChip is the Functional Programming Virtual Machine optimized for embedded system. It's 100% free software and free hardware design.

The Laco compiler is GPLv3, and the LambdaChip VM firmware is LGPLv3, the hardware design is CC-BY-SA.

[Learn more about LambdaChip](/articles/docs/0).

[Buy LambdaChip hardware to support us](https://shop.lambdachip.com).

## Changelog

### Enhanced global operations

In the previous version, the global variables and functions are forced to be inlined. Although it's functional, it's not flexible, and breaks Scheme language spec (R7RS).
In v0.1.0, the global objects are stored in a global table, and they can be referenced, applied or modified efficiently. The global functions may still possible be inlined
by Laco compiler in the optimization.

### A Better GPIO API design

```scheme
  (gpio-set! gpio-name value)
  (gpio-toggle! gpio-name)
  ```
The **gpio-name** should be a symbol, and the **value** is an integer or boolean.

For example:

```scheme
(define (main)
  (usleep 1000000)
  (gpio-set! 'dev_led2 1)
  (usleep 1000000)
  (gpio-set! 'dev_led2 0)
  (gpio-toggle! 'dev_led0)
  (gpio-toggle! 'dev_led1)
  (gpio-toggle! 'dev_led3)
  (main))
(main)
```

### Debug via BLE

If you want to debug Alonzo board on your smart phone via BLE, you can enable it:
```scheme
;; Put this line on top of your code
(enable-ble-debug)
```
If you have installed BLE tools on your smart phone, you can see the log like this screenshot:

<center>
<img src="/img/ble_debug.png" title="BLE debug screenshot" alt="BLE debug screenshot" height=600/>
</center>

### Fix dead-function-elimination

Laco compiler will eliminate the unused functions, this was broken in v0.0.3, now it's fixed.

### Fix module load path detection

There's a bug that Laco compiler will force to load modules from the system path even if you use **pre-inst-env**. In v0.1.0, Laco will detect the user defined path first, if it's failed, then load module from the system path.

### Fix symbol table offset computation

In v0.0.3, the symbol table offset was wrong, so no matter how many symbols in the program, it will only print the first one.

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco
```

## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## Buy LambdaChip hardware to support us!

[Buy LambdaChip hardware to support us](https://shop.lambdachip.com).

## End

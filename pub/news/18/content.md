Please fill the form to finish the survey:

[https://forms.gle/Kkw5gexeqHN3GFwa6](https://forms.gle/Kkw5gexeqHN3GFwa6)

We appreciate that you spend your precious time taking part in the survey. And you'll be rewarded when it's ending. **Please don't forget to leave your email in the survey form for the award**. Your privacy is respected, the email is only used for discount codes.

If you submit meaningful results, you can get a **10% discount code** which will be sent to your email.

If your advice is valuable, we'll award you **another 10% discount code**. That is to say, you'll get a 20% discount code.

The discount code can be used for any products at [https://shop.lambdachip.com](https://shop.lambdachip.com). Of course, you may keep the discount code when you find any favor product in the future.

The survey will **end on Aug 17th, 2021**.

If any issue, please send mail to help@lambdachip.com.

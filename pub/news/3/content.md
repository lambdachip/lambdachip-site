<center>
<img src="/img/v0.0.3.png" title="LambdaChip v0.0.3 released" alt="LambdaChip v0.0.3 released"/>
</center>

## Introduction

LambdaChip is the Functional Programming Virtual Machine optimized for embedded system.

[Learn more about LambdaChip](/articles/docs/0).

[Buy a LambdaChip hardware](https://shop.lambdachip.com).

## Changelog

- Fix deadlock in Garbage Collector
- Fix to properly handle the primitive passed to map/for-each
- Upgrade to ZephyrRTOS 2.5.0

## For developers

Don't forget to upgrade the environment:
```bash
docker pull registry.gitlab.com/lambdachip/lambdachip-zephyr:latest
```
Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## End

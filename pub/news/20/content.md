<center>
<img src="/img/release.png" title="LambdaChip v0.4.0 released" alt="LambdaChip v0.4.0 released"/>
</center>

## Introduction

LambdaChip is the modern advanced platform for embedded development. You can use functional programming paradigm to control hardware, make cool stuffs, or IoT prototyping.

The software of LambdaChip is FOSS, and its hardware is [certified open hardware](https://certification.oshwa.org/cn000009.html). Free as in freedom, not free beer.

## Changelog

### Add bytevector object

Bytevectors represent blocks of binary data. They are
fixed-length sequences of bytes, where a byte is an exact
integer in the range from 0 to 255 inclusive. A bytevector
is typically more space-efficient than a vector containing
the same values.

Currently, the supported bytevector primitives are:
- bytevector?
- make-bytevector
- bytevector-length
- bytevector-u8-ref
- bytevector-u8-set!
- bytevector-copy
- bytevector-copy!
- bytevector-append
- i2c-read-bytevector!

### Fix define* binding in the correct order

**define\*** is a syntax sugar in Laco compiler, it expands the default values of arguments to the let-binding syntax:

```scheme
(define b #u8(1 2 3))
(define bcopy (bytevector-copy b 1))
```
After the parser expanded it internally, the *bytevector-copy* application will be:
```scheme
(display
   (let ((bv b))
     (let ((start 1))
       (let ((end (bytevector-length bv)))
         (bytevector-copy bv start end)))))
```
### Upgrade to ZephyrRTOS v2.6.0
There was a bug in ZephyrRTOS that may block I2C operation. We upgrade ZephyrRTOS to v2.6.0 to fix it.

### Fix to prevent non-function be lifted

### Fix to prevent global segment to be freed after init

### Fix to flatten global objects-array

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco:latest
```
or
```bash
docker pull registry.gitlab.com/lambdachip/laco:0.4.1
```
## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## About us
Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is an IoT solution provider in Asia. We respect the value of FOSS (free-and-opensource sorftware). We also advocate Functional Programming, and bring it to embedded software development to reduce the complexity. We believe it helps to improve the quality of IoT software.

## Go to LambdaChip Shop!

Support FOSS and open hardware, join us!

[See our DevKit](https://shop.lambdachip.com), you don't have to sign-up an account to order.

## End

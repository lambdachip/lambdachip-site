<center>
<img src="/img/release.png" title="LambdaChip v0.4.3 released" alt="LambdaChip v0.4.3 released"/>
</center>

## Introduction

LambdaChip is the modern advanced platform for embedded development. You can use functional programming paradigm to control hardware, make cool stuffs, or IoT prototyping.

The software of LambdaChip is FOSS, and its hardware is [certified open hardware](https://certification.oshwa.org/cn000009.html). Free as in freedom, not free beer.

## Changelog

### Add primitive gpio-get

Get signal via GPIO, for example:

```scheme
(gpio-get 'dev_gpio_pa10)
```
### Add primitive vm-reset!

### Fix local definition

### Fix local closure

### Upgrade autotools

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco:latest
```
or
```bash
docker pull registry.gitlab.com/lambdachip/laco:0.4.3
```
## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## About us
Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is an IoT solution provider in Asia. We respect the value of FOSS (free-and-opensource sorftware). We also advocate Functional Programming, and bring it to embedded software development to reduce the complexity. We believe it helps to improve the quality of IoT software.

## Looking for IoT solution service?

We provide commercial [service of IoT solution](https://lambdachip.com/articles/misc/solution).

And please read [LambdaChip whitepaper](https://lambdachip.com/articles/misc/whitepaper).

## Go to LambdaChip Shop!

Support FOSS and open hardware, join us!

[See our DevKit](https://shop.lambdachip.com), you don't have to sign-up an account to order.

## End

<center>
<img src="/img/alonzo_pack_1.jpg" title="LambdaChip Alonzo pack" alt="LambdaChip Alonzo pack"/>
</center>

## Play Alonzo out of the box!

Alonzo box is the complete version of Alonzo board. It contains everything you need to play Alonzo, so you save your time to find the necessary parts to make it work.
Now let's see what's in the box.

## Alonzo board with Acrylic cover

Of course, there must be an Alonzo board. The standard Alonzo board PCB is matte texture, although it looks good, it's easy to leave your fingerprint when you touch it. We provided the Acrylic cover to protect the Alonzo board, and keep it clean.

<center>
<img src="/img/alonzo_acrylic_cover.jpg" alt="Acrylic cover" title="Acrylic cover" width="450"/>
<img src="/img/alonzo_acrylic_in_pack.jpg" alt="Alonzo with Acrylic cover" title="Alonzo with Acrylic cover"  width="450"/>
</center>

## The Saruman debugger

Saruman debugger is the [Black Magic Probe](https://www.adafruit.com/product/3839) compatible debugger. It allows you see what is going on 'inside' an application running on a MCU while it executes. It is able to control and examine the state of the target MCU using a JTAG cable. The users can choose whatever IDE they like to debug Alonzo board, Emacs, Vim, VS Code, etc. Because Saruman supports GNU Debugger (GDB), that's the reason why it supports so many cool IDE, if and only if you configure GDB in your IDE correctly.

In addition, Saruman is not only able to debug Alonzo board, it actually supports to debug many MCU:
- STM32 F0, F1, F2, F3, F4, F7
- Atmel SAMD20/21, SAM3N/S/X/U, SAM4L, SAM4S
- Nordic nRF51, nRF52
- NXP LPC8xx, LPC11xx, LPC15xx, LPC43xx
- TI LM3S, TM4C
- Freescale KL25, KL27, KL02
- Xilinx Zynq-7000
- Broadcom BCM2836 (Raspberry Pi 2)
- SiLabs EFM32 and EZR32

<center>
<img src="/img/saruman_coin.png" title="Saruman debugger" alt="Saruman debugger" width="800"/>
<img src="/img/saruman_vscode.png" title="Debug in VS code" alt="Debug in VS code" width="1000"/>
</center>

## Cables and converter

Alonzo board has a onboard BLE (Bluetooth Low Energy) chip to help users connect and debug via wireless. However, it doesn't mean users don't need USB cable anymore. Sometimes it's still useful.

Alonzo board can be powered in 2 alternative ways. The first way is to connect Alonzo with a Lithium Ion battery which is not provided in the Alonzo box for shipping constrains; the second way is to connect Alonzo to a power bank with TypeC-2-TypeC cable. For some situations that users may not have typeC interface on the power bank or PC, we provide USB_A-2-TypeC converter. BTW, the TypeC-2-TypeC cable supports quick charge that you can use for your smart phone.

<img src="/img/t-cable.jpg" title="TypeC-2-TypeC cable" alt="TypeC-2-TypeC" width="450"/>
<img src="/img/t2u-converter.jpg" title="USB_A-2-TypeC converter" alt="TypeC-2-USB converter"  width="450"/>

## Last but not least

Alonzo board loads the program from TF card, so users have to prepare it. In theory, you can't make Alonzo board work without TF card. Don't worry, we provide it for you in Alonzo complete version.

<center>
<img src="/img/8G-tf-card.jpg" title="TF card" alt="TF card" width="450"/>
</center>


## What's next?

Take a look at [our shop](https://shop.lambdachip.com)

We strongly recommend you to learn about the basics of LambdaChip, please click here to [Get Started](/articles/docs/0)

## End

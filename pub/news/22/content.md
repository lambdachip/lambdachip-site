<center>
<img src="/img/release.png" title="LambdaChip v0.4.2 released" alt="LambdaChip v0.4.2 released"/>
</center>

## Introduction

LambdaChip is the modern advanced platform for embedded development. You can use functional programming paradigm to control hardware, make cool stuffs, or IoT prototyping.

The software of LambdaChip is FOSS, and its hardware is [certified open hardware](https://certification.oshwa.org/cn000009.html). Free as in freedom, not free beer.

## Changelog

### Add more primitives

- i2c-write-bytevector!
- floor
- floor/
- ceiling
- truncate
- round
- rationalize
- floor-quotient
- floor-remainder
- truncate/
- truncate-quotient
- truncate-remainder
- numerator
- denominator
- exact-integer?
- finite?
- infinite?
- nan?
- zero?
- positive?
- negative?
- odd?
- even?
- square
- sqrt
- exact-integer-sqrt
- expt

There's a new helper function **pk** for debug.

```scheme
(pk 123)
;;; 123
123
```

### Fix to replace #vu8 with #u8 to comply with R7RS

### Fix lambda application with multi-args

### Fix logic special form to work correctly

Fix **and**, **or** special form to work:

```scheme
(display (and))
(display (and 1))
(display (and 1 2))
(display (and (+ 1 2) (< 2 1)))

(display (or))
(display (or 1))
(display (or #f 2))
(display (or (< 2 1) (+ 1 2)))

```

## For LambdaChip makers

Please upgrade the Laco compiler:
```bash
docker pull registry.gitlab.com/lambdachip/laco:latest
```
or
```bash
docker pull registry.gitlab.com/lambdachip/laco:0.4.2
```
## For advanced contributors

Please visit [How to be a contributor](/articles/docs/8) for more details.

## Download

Please visit [download page](/articles/misc/download).

## Flash firmware

Please visit [firmware upgrade guide](/articles/docs/10/).

## About us
Tuwei Technology (Shenzhen) Co.,LTD (图桅科技) is an IoT solution provider in Asia. We respect the value of FOSS (free-and-opensource sorftware). We also advocate Functional Programming, and bring it to embedded software development to reduce the complexity. We believe it helps to improve the quality of IoT software.

## Looking for IoT solution service?

We provide commercial [service of IoT solution](https://lambdachip.com/articles/misc/solution).

And please read [LambdaChip whitepaper](https://lambdachip.com/articles/misc/whitepaper).

## Go to LambdaChip Shop!

Support FOSS and open hardware, join us!

[See our DevKit](https://shop.lambdachip.com), you don't have to sign-up an account to order.

## End

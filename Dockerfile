FROM        registry.gitlab.com/nalaginrut/artanis:latest
MAINTAINER  Mu Lei
ENV         LANG C.UTF-8

ARG CACHEBUST=1
RUN set -ex \
        && git clone --depth 1 https://gitlab.com/lambdachip/lambdachip-site.git

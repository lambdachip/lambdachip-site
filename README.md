# The site of https://lambdachip.com

## Development


```shell

# development build and watch
$ yarn start

# start artanis backend server
$ yarn dev:server

```

Then access `http://localhost:3000/index/` for the home page.



## Production


```
# build JS production bundle 
$ yarn build

```

;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2021
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Rem is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Rem is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.

(define-module (rem utils)
  #:use-module (artanis artanis)
  #:use-module (artanis utils)
  #:use-module (artanis irregex)
  #:use-module (srfi srfi-19)
  #:use-module ((rnrs) #:select (get-string-all))
  #:export (get-article-meta-from
            gen-news
            fix-decode
            timestamp->date
            timestamp->atom-date))

(define* (get-article-meta-from type #:optional (count #f) (mode 'oldest-top))
  (define (gen-file n) (format #f "~a/pub/~a/~a" (proper-toplevel) type n))
  (define (get-meta filename)
    (let ((meta-file (string-append filename "/meta.json")))
      (when (not (file-exists? meta-file))
        (throw 'artanis-err 500 get-meta
               "`~a' doesn't exist!" meta-file))
      (json-string->scm (call-with-input-file meta-file get-string-all))))
  (define (gen-vec lst)
    (let* ((len (length lst))
           (vec (make-vector len)))
      (for-each (lambda (x i) (vector-set! vec i x)) lst (iota len))
      vec))
  (define ret
    (case mode
      ((latest-top) identity)
      ((oldest-top) reverse)
      (else (throw 'artanis-err 500 get-article-meta-from
                   "Invalid mode `~a'" mode))))
  (define (list-limit lst cnt)
    (if (not cnt)
        lst
        (list-head lst (min (length lst) cnt))))
  (let lp ((n 0) (ml '()))
    (let ((filename (gen-file n)))
      (cond
       ((not (file-exists? filename))
        (gen-vec (list-limit (ret ml) count)))
       (else
        (let ((meta (get-meta filename)))
          (lp (1+ n) (cons meta ml))))))))

(define (gen-news cnt)
  (get-article-meta-from 'news cnt 'latest-top))

(define (fix-decode s)
  (irregex-replace/all
   "&nbsp;"
   (irregex-replace/all "<p><br></p>" s "")
   "&#160;"))

(define (timestamp->date timestamp)
  (time-utc->date (make-time time-utc 0 timestamp) 0))

(define (timestamp->atom-date timestamp)
  (date->string (timestamp->date timestamp)
                "~Y-~m-~dT~H:~M:~SZ"))

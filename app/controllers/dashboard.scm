;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2020
;;      "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
;;  Rem is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License
;;  published by the Free Software Foundation, either version 3 of
;;  the License, or (at your option) any later version.

;;  Rem is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program. If not, see <http://www.gnu.org/licenses/>.

;; This file is generated automatically by GNU Artanis.
(define-artanis-controller dashboard) ; DO NOT REMOVE THIS LINE!!!
(use-modules (ice-9 iconv)
             (srfi srfi-1)
             (srfi srfi-11)
             (artanis utils)
             (artanis artanis))

(define (gen-login-page rc)
  (let ((failed (params rc "failed")))
    (view-render "login" (the-environment))))

(dashboard-define
 "/"
 (lambda (rc)
   (view-render "index" (the-environment))))

(dashboard-define
 "editor"
 (lambda (rc)
   (view-render "editor" (the-environment))))

(dashboard-define
 "news_editor"
 (lambda (rc)
   (view-render "news_editor" (the-environment))))

(dashboard-define
 "post"
 ;;(options #:with-auth gen-login-page)
 (method post)
 (lambda (rc)
   "ok"))

(dashboard-define
 "upload"
 (lambda (rc)
   (view-render "upload" (the-environment))))

;; (define (gen-login-page rc)
;;   (let ((failed (params rc "failed")))
;;     (view-render "login" (the-environment))))

;; (dashboard-define
;;  new_post
;;  (options #:with-auth gen-login-page)
;;  (lambda (rc)
;;    (let ((author (object->string (colt-conf-get 'blog-author))))
;;      (view-render "new_post" (the-environment)))))

;; (dashboard-define
;;  edit
;;  (options #:with-auth gen-login-page)
;;  (lambda (rc)
;;    (let ((post-content (tpl->html (generate-editable-index-content "edit"))))
;;      (view-render "edit" (the-environment)))))

;; (dashboard-define
;;  setting
;;  (options #:with-auth gen-login-page)
;;  (lambda (rc)
;;    (let ((title (object->string (colt-conf-get 'blog-name)))
;;          (author (object->string (colt-conf-get 'blog-author)))
;;          (blog-url (object->string (colt-conf-get 'blog-url)))
;;          (disqus (object->string (colt-conf-get 'disqus)))
;;          (disqus-pubkey (object->string (colt-conf-get 'disqus-pubkey))))
;;      (view-render "setting" (the-environment)))))

;; (dashboard-define
;;  intro
;;  (options #:with-auth gen-login-page)
;;  (lambda (rc)
;;    (let ((intro-content (tpl->html (generate-editable-intro-content "edit"))))
;;      (view-render "intro" (the-environment)))))

;; (dashboard-define
;;  cards
;;  (lambda (rc)
;;    (view-render "cards" (the-environment))))

;; (dashboard-define
;;  logout
;;  (options #:session #t)
;;  (lambda (rc)
;;    (pk "drop"(:session rc 'drop))
;;    (pk "cookie" (rc-set-cookie rc))
;;    (redirect-to rc "/")))

;; (dashboard-define
;;  /
;;  (options #:with-auth "dashboard/login")
;;  (lambda (rc)
;;    (let ((account (object->string (colt-conf-get 'disqus))))
;;      (view-render "dashboard" (the-environment)))))

;; (dashboard-define
;;  login
;;  gen-login-page)

;; (post "/v1/colt/post_article"
;;   #:with-auth "dashboard/login"
;;   (lambda(rc)
;;     ;;(DEBUG "rc: ~a~%" (route-context? rc))
;;     (let* ((json-body (bytevector->string (rc-body rc) "utf-8"))
;;            (json-content (json-string->scm json-body))
;;            (article-title (hash-ref json-content "title"))
;;            (article-author (hash-ref json-content "author"))
;;            (article-tags (hash-ref json-content "tags"))
;;            (article-content (hash-ref json-content "content"))
;;            (status (hash-ref json-content "status"))
;;            (timestamp ((@(guile) current-time)))
;;            (url-name (uri-encode (gen-proper-url-name timestamp article-title)))
;;            (article-meta `(("timestamp" . ,timestamp)
;;                            ("tags" . ,article-tags)
;;                            ("status" . ,(if (string-null? status) "publish" status))
;;                            ("title"  . ,article-title)
;;                            ("name"  . ,article-author)
;;                            ;; Always close here, we're going to use 3rd-party commenting system.
;;                            ("comment_status" . "closed")))
;;            (oid (git-post-article url-name article-content
;;                                   article-meta)))
;;       (update-index-posts-cache oid 'post)
;;       oid)))

;; (post "/v1/colt/edit_article"
;;   #:with-auth "dashboard/login"
;;   (lambda(rc)
;;     ;;(DEBUG "rc: ~a~%" (route-context? rc))
;;     (let* ((json-body (bytevector->string (rc-body rc) "utf-8"))
;;            (json-content (json-string->scm json-body))
;;            (article-title (hash-ref json-content "title"))
;;            (article-name (hash-ref json-content "name"))
;;            (article-content (hash-ref json-content "content"))
;;            (article-status (hash-ref json-content "status"))
;;            (article-oid (hash-ref json-content "oid"))
;;            (article-tags (hash-ref json-content "tags"))
;;            (update-timestamp? (hash-ref json-content "update_timestamp"))
;;            (oid (git-edit-article article-title article-name article-content
;;                                   article-oid article-tags article-status
;;                                   update-timestamp?)))
;;       (update-index-posts-cache oid 'edit)
;;       oid)))

;; (post "/v1/colt/remove_article"
;;   #:with-auth "dashboard/login"
;;   (lambda(rc)
;;     ;;(DEBUG "rc: ~a~%" (route-context? rc))
;;     (let* ((json-body (bytevector->string (rc-body rc) "utf-8"))
;;            (json-content (json-string->scm json-body))
;;            (article-title (hash-ref json-content "title"))
;;            (article-content (hash-ref json-content "content"))
;;            (article-oid  (hash-ref json-content "oid"))
;;            (status (git-remove-article article-title article-content
;;                                        article-oid)))
;;       (update-index-posts-cache article-oid 'remove)
;;       (if (string=? status "failed")
;;           "failed"
;;           "ok"))))

;; (post "/v1/colt/change_setting"
;;   #:with-auth "dashboard/login" #:from-post #t
;;   (lambda (rc)
;;     (let-values (((title author blog disqus pubkey old-passwd new-passwd)
;;                   (:from-post rc 'get-vals "title" "author" "blog-url"
;;                    "disqus" "disqus-pubkey" "old-passwd" "new-passwd")))
;;       (let/ec return
;;         (when (and new-passwd (not (string-null? new-passwd)))
;;           (if (check-passwd old-passwd)
;;               (reset-passwd new-passwd)
;;               (return (scm->json-string '((status . 401) (reason . "Old password is wrong!"))))))
;;         (colt-conf-set! 'blog-name title)
;;         (colt-conf-set! 'blog-author author)
;;         (colt-conf-set! 'blog-url blog)
;;         (colt-conf-set! 'disqus disqus)
;;         (colt-conf-set! 'disqus-pubkey pubkey)
;;         (colt-conf-dump!)
;;         (scm->json-string '((status . 200) (reason . "Settings saved succefully!")))))))

;; (post "/v1/colt/upload_head_img"
;;   #:with-auth "dashboard/login"
;;   #:from-post `(store #:path ,(format #f "~a/pub/img/" (current-toplevel)))
;;   (lambda (rc)
;;     (:from-post rc 'store)
;;     "ok"))

;; Controller news definition of lambdachip-site
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller news) ; DO NOT REMOVE THIS LINE!!!

(import (rem utils))

(news-define
 "latest/:count"
 (options #:mime 'json)
 (lambda (rc)
   (let* ((count (string->number (params rc "count")))
          (news (gen-news count))
          (ret `(("news" . ,news))))
     (:mime rc ret))))

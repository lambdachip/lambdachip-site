;; Controller feed definition of lambdachip-site
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller feed) ; DO NOT REMOVE THIS LINE!!!

(import (rem utils)
        (web uri)
        (rnrs))

(define (atom-entry meta)
  (define-syntax-rule (->url)
    (format #f "https://lambdachip.com/articles/news/~a" (assoc-ref meta "oid")))
  (let ((title (assoc-ref meta "title"))
        (updated-time (timestamp->atom-date
                       (assoc-ref meta "date")))
        (author "LambdaChip")
        (url (->url)))
    `(entry
      (author (name ,author) (uri "https://lambdachip.com"))
      (title (@ (type "text")) ,(if (string-null? title) "No title" title))
      ;; id must be a full and valid URL
      (id ,url)
      (link (@ (rel "alternate") (type "text/html")
               (href ,url)))
      (published ,updated-time)
      (updated ,updated-time)
      (summary ,(assoc-ref meta "abstract"))
      (content (@ (type "xhtml"))
               (div (@ (xmlns "http://www.w3.org/1999/xhtml"))
                    "Visit the link to read.")))))

(define (news->atom news)
  (tpl->html (map (lambda (i)
                    (atom-entry (vector-ref news i)))
                  (iota (vector-length news)))))

(feed-define
 atom
 (options #:cache #t)
 (lambda (rc)
   (let* ((news (gen-news 50))
          (update-time (timestamp->atom-date
                        (assoc-ref (vector-ref news 0) "date"))))
     (response-emit
      (let ((entries (news->atom news)))
        (view-render "atom" (the-environment)))
      #:headers '((content-type application/atom+xml))))))

;; Controller articles definition of rem
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller articles) ; DO NOT REMOVE THIS LINE!!!

(import (rem utils))

(articles-define
 "docs/all"
 (options #:mime 'json)
 (lambda (rc)
   (:mime rc `(("docs" . ,(get-article-meta-from 'docs))))))

(articles-define
 "docs"
 (lambda (rc)
   (let ((category "docs"))
     (view-render "toc" (the-environment)))))

(articles-define
 "news"
 (lambda (rc)
   (let ((category "news"))
     (view-render "news" (the-environment)))))

(articles-define
 "docs/:oid"
 (lambda (rc)
   (let* ((oid (params rc "oid"))
          (category "docs"))
     (view-render "docs" (the-environment)))))

(articles-define
 "misc/:name"
 (lambda (rc)
   (let* ((name (params rc "name"))
          (category "misc"))
     (pk "yes")
     (view-render "misc" (the-environment)))))

(articles-define
 "news/:oid"
 (lambda (rc)
   (let* ((oid (params rc "oid"))
          (category "news"))
     (view-render "docs" (the-environment)))))

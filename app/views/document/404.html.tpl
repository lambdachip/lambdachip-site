<!DOCTYPE html>
<html>
    <head>
        <@include header.html.tpl %>
    </head>

    <body>
        <@include common_nav.html.tpl %>
        <div class="main-content container">
            <div id="card-alert" class="small card blue lighten-5">
                <div class="card-content blue-text">
                    <span class="card-title blue-text">Page not found 404</span>
                    <p> Oops! Seems you come to the Wasteland finally ... </p>
                    <p> If I were you, I'd go around for something interesting ... </p>
                </div>
                <div class="center card-action blue lighten-3">
                    <a href="/" class="red-text">I see, let me get out of here!</a>
                </div>
            </div>
        </div>
    </body>
</html>

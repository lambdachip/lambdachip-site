<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Hello React!</title>
    </head>
    <body>
        <div id="example"></div>

        <@include header.html.tpl %>

        <!-- Main -->
        <@js index.js %>
    </body>
</html>

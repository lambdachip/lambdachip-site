<html>
    <head>
        <@include header.html.tpl %>
        <@css font.css %>
        <@css doc.css %>
        <@js editor_vendor.js %>
        <@js vendor/highlight.pack.js %>
    </head>
    <body>
        <div id="article-container">
            <div id="article-content"
                 oid=<%= name %>
                 category=<%= category %>>
            </div>
        </div>
        <@js docs.js %>
    </body>
</html>

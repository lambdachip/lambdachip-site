<html>
    <head>
        <@include header.html.tpl %>
        <@css doc.css %>
        <@css font.css %>
        <@js editor_vendor.js %>
        <@js vendor/highlight.pack.js %>
    </head>
    <body>
        <div id="article-container">
            <div id="article-content"
                 category=<%= category %>>
            </div>
        </div>
        <@js toc.js %>
    </body>
    <script>
     // setTimeout(() => {
     //     document.getElementById("doc-content").querySelectorAll("a").forEach(e=>e.style.color='#ff6f61');
     // }, 1000);
    </script>
</html>

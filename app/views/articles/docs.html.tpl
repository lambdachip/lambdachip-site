<html>
    <head>
        <@include header.html.tpl %>
        <@css font.css %>
        <@css doc.css %>
        <@css monokai-sublime.min.css %>
        <@js editor_vendor.js %>
        <@js vendor/highlight.pack.js %>
    </head>
    <body>
        <div id="article-container">
            <div id="article-content"
                 oid=<%= (object->string oid) %>
                 category=<%= category %>>
            </div>
        </div>
        <@js docs.js %>
    </body>
    <script>
     // setTimeout(() => {
     //     hljs.configure({
     //         tabReplace: '  ', // 2 spaces
     //     });
     //     // don't query something is not pre-rendered
     //     document.querySelectorAll("code").forEach(block => {
     //         hljs.highlightBlock(block);
     //     });
     //     // don't query something is not pre-rendered
     //     // document.getElementById("doc-content").querySelectorAll("a").forEach(e=>e.style.color='#ff6f61');
     // }, 2500);
    </script>
</html>

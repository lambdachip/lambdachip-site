<html>
    <head>
        <@include header.html.tpl %>
        <@css doc.css %>
    </head>
    <body>
        <div id="article-container">
            <div id="article-content"
                 category=<%= category %>>
            </div>
        </div>
    </body>
    <@js news.js %>
</html>

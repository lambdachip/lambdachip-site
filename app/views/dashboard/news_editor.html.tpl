<!DOCTYPE html>
<html>
    <head>
        <@include header.html.tpl %>
        <@js editor_vendor.js %>
        <@css editor.css %>
        <@css ../../node_modules/simplemde/dist/simplemde.min.css %>
    </head>

    <body>
        <div id="editor_root"></div>
    </body>
    <@js editor.js %>
</html>

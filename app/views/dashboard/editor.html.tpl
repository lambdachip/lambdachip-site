<html>
    <head>
        <@include header.html.tpl %>
        <@css editor.css %>
        <@js editor_vendor.js %>
        <@css ../../node_modules/simplemde/dist/simplemde.min.css %>
    </head>
    <body>
        <div class="main-content">
            <div id="root"></div>
        </div>
    </body>
    <@js editor.js %>
</html>
